from rest_framework import serializers

from player.models import Player
from room.models import Room


class RoomSerializer(serializers.ModelSerializer):
    players = serializers.SerializerMethodField('get_players')
    game_id = serializers.SerializerMethodField('get_game_id')
    owner = serializers.SerializerMethodField('get_owner')

    def get_players(self, obj):
        if (Player.objects.filter(room=obj) == None):
            return []
        else:
            return [i.user.username for i in Player.objects.filter(room=obj)]

    def get_game_id(self, obj):
        if obj.game != None:
            return obj.game.id
        else:
            return ''

    def get_owner(self, obj):
        return Player.objects.get(room=obj, isGameOwner=True).user.username

    class Meta:
        model = Room
        fields = (
            'id', 'name', 'owner', 'players', 'max_players',
            'game_has_started',
            'game_id')
