from django.contrib.auth import authenticate
from django.test import TestCase
from django.test import tag
from rest_framework.test import APIClient
from rest_framework.utils import json
from rest_framework_simplejwt.tokens import RefreshToken

from board.models import Board
from game.models import Game
from player.models import Player
from room.models import Room
from session.models import Session
from user.models import User


class getRoomsTest(TestCase):
    """
    Para agilizar testeo hay dos clases de test: 'DEBUG' y 'LIGHT'
    - 'LIGHT': Solo hace asserts de los test e imprime si los tests pasan.
    - 'DEBUG': Debug prueba todos los test e imprime en consola lo que los
       test hicieron para mas detalle a la hora de debuguear.
    """
    user = None

    @tag('LIGHT')
    def setUp(self):
        self.get_room = APIClient().get('')
        self.create_room = APIClient().get('')
        self.get_one_room = APIClient().get('')
        # Room, Game y Board 1
        self.board = Board.objects.create(name='board')
        self.room = Room.objects.create(name='The Settlers', max_players=4,
                                        state='Available',
                                        game_has_started=False)
        self.game = Game.objects.create(name='gm', currentTurn=1,
                                        board=self.board)
        Room.objects.filter(id=self.room.id).update(game=self.game)
        # Room, Game y Board 2
        self.board = Board.objects.create(name='board')
        self.room2 = Room.objects.create(name='Ovejitas', max_players=4,
                                         state='Available',
                                         game_has_started=False)
        self.game2 = Game.objects.create(name='gm', currentTurn=1,
                                         board=self.board)
        Room.objects.filter(id=self.room2.id).update(game=self.game2)
        # Usuarios
        self.user = User.objects.create_user(username='pepe',
                                             password='123qwe')
        self.user2 = User.objects.create_user(username='hercules',
                                              password='123qwe')
        self.user3 = User.objects.create_user(username='toshi',
                                              password='123qwe')
        self.user4 = User.objects.create_user(username='ernesto',
                                              password='123qwe')

        # Session 1
        self.client = APIClient()
        self.user = authenticate(username=self.user.username,
                                 password='123qwe')
        accessToken = (RefreshToken.for_user(self.user)).access_token
        self.session = Session.objects.create(
            tRefresh=(RefreshToken.for_user(self.user)), tAccess=accessToken,
            user=self.user)
        self.player = Player.objects.create(user=self.user, room=self.room,
                                            colour='Azul',
                                            inTurn=True, game=self.game,
                                            isGameOwner=True)
        self.playerr = Player.objects.create(user=self.user, room=self.room2,
                                             colour='Azul',
                                             inTurn=True, game=self.game2,
                                             isGameOwner=True)
        # Sesion 2
        self.user2 = authenticate(username=self.user2.username,
                                  password='123qwe')
        accessToken2 = (RefreshToken.for_user(self.user2)).access_token
        self.session2 = Session.objects.create(
            tRefresh=(RefreshToken.for_user(self.user2)), tAccess=accessToken2,
            user=self.user2)
        self.player2 = Player.objects.create(user=self.user2, colour='Rojo',
                                             inTurn=False, game=self.game,
                                             room=self.room)
        self.player2r = Player.objects.create(user=self.user2, colour='Rojo',
                                              inTurn=False, game=self.game2,
                                              room=self.room2)
        # Sesion 3
        self.user3 = authenticate(username=self.user3.username,
                                  password='123qwe')
        accessToken3 = (RefreshToken.for_user(self.user3)).access_token
        self.session3 = Session.objects.create(
            tRefresh=(RefreshToken.for_user(self.user3)), tAccess=accessToken2,
            user=self.user3)
        self.player3 = Player.objects.create(user=self.user3,
                                             colour='Amarillo',
                                             inTurn=False, game=self.game,
                                             room=self.room)

        self.player3r = Player.objects.create(user=self.user3, colour='Rojo',
                                              inTurn=False, game=self.game2,
                                              room=self.room2)
        # Sesion 4
        self.user4 = authenticate(username=self.user4.username,
                                  password='123qwe')
        accessToken4 = (RefreshToken.for_user(self.user4)).access_token
        self.session4 = Session.objects.create(
            tRefresh=(RefreshToken.for_user(self.user4)), tAccess=accessToken2,
            user=self.user4)
        self.player4 = Player.objects.create(user=self.user4,
                                             colour='Amarillo',
                                             inTurn=False, game=self.game2,
                                             room=self.room2)

        self.token = str(accessToken)
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    @tag('LIGHT')
    def test_get_room_endpoint(self):
        response = self.client.get('/rooms/')
        self.get_room = response
        assert response.status_code == 200
        print('\033[32m', "Test room (1/10) --> get_room_endpoint: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_create_room_endpoint(self):
        response = self.client.post('/rooms/', {'name': 'Caballeros Rojos',
                                                'board_id': self.board.id})
        self.create_room = response
        assert response.status_code == 201
        print('\033[32m', "Test room (2/10) --> create_room_endpoint: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_create_room_content(self):
        response = self.client.post('/rooms/', {'name': 'Caballeros Rojos',
                                                'board_id': self.board.id})
        self.create_room = response
        game = Game.objects.get(name='Caballeros Rojos', board=self.board)
        room = Room.objects.get(name='Caballeros Rojos', game=game)
        assert Player.objects.get(user=self.user, isGameOwner=True, room=room,
                                  game=game).room.name == 'Caballeros Rojos'
        print('\033[32m', "Test room (3/10) --> create_room_content: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_get_one_room(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(self.token))
        response = self.client.get('/rooms/2/')
        self.get_one_room = response
        assert response.status_code == 200
        assert Room.objects.filter(id=self.room2.id).count() == 1
        print('\033[32m', "Test room (4/10) --> get_one_room: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_get_one_room_does_not_exist(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(self.token))
        response = self.client.get('/rooms/13/')
        self.get_one_room = response
        assert response.status_code == 404
        assert not Room.objects.filter(id=13).exists()
        print('\033[32m',
              "Test room (5/10) --> get_one_room_does_not_exist: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_update_room_player_exists(self):
        response = self.client.put('/rooms/' + str(self.room.id) + '/')
        # el usuario es pepe y ya esta en la sala.
        self.update_room = response
        assert response.status_code == 409
        assert Player.objects.filter(user__exact=self.session.user,
                                     room=self.room).exists() == True
        print('\033[32m', "Test room (6/10) --> update_room_player_exists: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_update_room(self):
        # Se actualiza el cuarto jugador.
        accessToken = (RefreshToken.for_user(self.user4)).access_token
        self.session4 = Session.objects.create(
            tRefresh=(RefreshToken.for_user(self.user4)), tAccess=accessToken,
            user=self.user4)
        self.client.credentials(
            HTTP_AUTHORIZATION='Bearer ' + str(accessToken))
        # Antes de llamar a put, hay tres jugadores.
        assert Player.objects.filter(room=self.room).count() == 3
        response = self.client.put('/rooms/' + str(self.room.id) + '/')
        self.update_room = response
        assert response.status_code == 202
        # Después de llama al put, hay un jugador mas.
        assert Player.objects.filter(room=self.room).count() == 4
        print('\033[32m', "Test room (7/10) --> update_room: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_update_room_five_players(self):
        # Se crea un quinto jugador.
        self.user5 = User.objects.create_user(username='lucy',
                                              password='123qwe')
        self.user5 = authenticate(username=self.user5.username,
                                  password='123qwe')
        accessToken = (RefreshToken.for_user(self.user5)).access_token
        self.session5 = Session.objects.create(
            tRefresh=(RefreshToken.for_user(self.user5)), tAccess=accessToken,
            user=self.user5)
        self.client.credentials(
            HTTP_AUTHORIZATION='Bearer ' + str(accessToken))
        # Antes de llamar a put, hay cuatro jugadores.
        assert Player.objects.filter(room=self.room2).count() == 4
        response = self.client.put('/rooms/' + str(self.room2.id) + '/')
        self.update_room = response
        assert response.status_code == 409
        # Después de llama al put, sigue teniendo cuatro jugadores.
        assert Player.objects.filter(room=self.room2).count() == 4
        print('\033[32m', "Test room (8/10) --> update_room_five_players: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_start_game(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(self.token))
        # Antes de llamar a patch la partida no esta comenzada.
        assert not Room.objects.filter(id=self.room.id,
                                       game_has_started=True).exists()
        response = self.client.patch('/rooms/1/')
        assert response.status_code == 202
        # Despues de patch la partida comienza.
        assert Room.objects.filter(id=self.room.id,
                                   game_has_started=True).exists()
        print('\033[32m', "Test room (9/10) --> start_game: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_delete_room(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(self.token))
        # Antes de llamar a delete la sala existe.
        assert Room.objects.filter(id=self.room.id).exists()
        response = self.client.delete('/rooms/1/')
        self.delete_room = response
        assert response.status_code == 204
        # Despues de llamar a delete, ya no existe la sala.
        assert not Room.objects.filter(id=self.room.id).exists()
        print('\033[32m', "Test room (10/10) --> delete_room: OK",
              '\033[0m', sep='')

    @tag('DEBUG')
    def test_debug_mode(self):
        self.test_get_room_endpoint()
        self.test_create_room_endpoint()
        self.test_get_one_room()
        print(json.dumps(self.get_room.json(), indent=4))
        print(json.dumps(self.create_room.json(), indent=4))
        print(json.dumps(self.get_one_room.json(), indent=4))
