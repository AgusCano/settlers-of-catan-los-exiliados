from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from room import views

urlpatterns = [
    path('rooms/', views.SalaList.as_view()),
    path('rooms/<int:pk>/', views.SalaUpdate.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
