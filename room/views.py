import random

from django.http import Http404
from rest_framework import mixins, generics, status
from rest_framework.response import Response
from rest_framework.views import APIView

from board.models import Board
from catan_configuration.game_elements import GameElements
from game.models import Game
from player.models import Player
from room.models import Room
from room.serializers import RoomSerializer
from session.models import Session


class SalaList(mixins.ListModelMixin,
               mixins.CreateModelMixin,
               generics.GenericAPIView):
    """
    Lista todas las salas.
    """
    queryset = Room.objects.all().order_by('id')
    serializer_class = RoomSerializer

    def get(self, request, *args, **kwargs):
        """
        Lista todas las salas.
        """
        rooms = list(Room.objects.all())
        serializer = RoomSerializer(rooms, many=True, read_only=True)
        return Response(serializer.data)

    def post(self, request):
        """
        Crea una nueva room.
        """
        data = request.data
        board = Board.objects.get(id=int(data['board_id']))
        room = Room(name=data['name'])
        room.save()
        game = Game(name=room.name, board=board)
        game.save()
        room.game = game
        room.save()
        player = Player(user=request.user,
                        isGameOwner=True,
                        room=room,
                        colour='yellow',
                        game=game)
        player.save()

        serializer = RoomSerializer(room)

        return Response(
            serializer.data,
            status=status.HTTP_201_CREATED,
        )


class SalaUpdate(APIView):
    """
    Actualiza una sala.
    """

    def get(self, request, pk, format=None):
        """
        Obtiene una sala.
        """
        try:
            room = Room.objects.get(pk=pk)
            serializer = RoomSerializer(room)
            return Response(serializer.data)
        except Room.DoesNotExist:
            raise Http404

    def put(self, request, pk):
        """
        Crea un jugador a partir de un usuario.
        Asocia ese jugador a una sala.
        """
        name_user = request.user

        # Obtiene la sala en donde se va a unir el jugador.
        try:
            room = Room.objects.get(pk=pk)
        except Room.DoesNotExist:
            raise Http404

        # Si el juego esta empezado, no se puede unir.
        if Room.objects.filter(pk=pk, game_has_started=True).count() > 0:
            return Response("El juego ya empezo",
                            status=status.HTTP_409_CONFLICT)

        # Si hay 4 o mas jugadores, hay error.
        playersInRoom = Player.objects.filter(room=room)
        if playersInRoom.count() >= 4:
            return Response("Maximo cuatro jugadores",
                            status=status.HTTP_409_CONFLICT)

        # Si el player ya está en la sala, no puede agregarlo, da error
        player = Player.objects.filter(user__exact=name_user,
                                       room=room).exists()
        if player:
            return Response("El jugador ya esta en la sala",
                            status=status.HTTP_409_CONFLICT)

        # De los colores que existen, se queda con los colores disponibles.
        colors = [k[1] for k in GameElements.COLOUR]
        availableColors = [j for j in colors if j not in [i.colour for i in
                                                          list(
                                                              Player.objects.filter(
                                                                  room=room))]]

        # Crea un jugador en esa sala y en el juego asociado a la sala.
        player = Player(user=request.user,
                        room=room,
                        colour=random.choice(availableColors),
                        game=room.game)
        player.save()
        return Response("Sala creada", status=status.HTTP_202_ACCEPTED)

    def patch(self, request, pk):
        """
        Inicia una partida
        """
        name_user = request.user
        room = Room.objects.get(id=pk)
        game = Game.objects.get(id=room.game.id)

        # Error si el jugador que inicia la partida no es el que la creó.
        owner = Player.objects.filter(room=room,
                                      isGameOwner=True).first().user
        if owner != name_user:
            return Response("No tiene permisos para iniciar la partida",
                            status=status.HTTP_401_UNAUTHORIZED)

        # Si no hay 3 o 4 jugadores, no se puede empezar la partida.
        count = Player.objects.filter(room=room).count()
        if count not in [3, 4]:
            return Response("Cantidad invalida de jugadores",
                            status=status.HTTP_409_CONFLICT)

        playerRequester = Player.objects.get(user__exact=name_user,
                                             room=room)
        room.state = GameElements.ROOM_STATUS[1][0]
        room.game_has_started = True
        room.save()
        # El dueño de la sala es el primer jugador.
        playerRequester.inTurn = True
        playerRequester.save()
        game.currentTurn = 1
        game.save()
        return Response("Partida iniciada", status=status.HTTP_202_ACCEPTED)

    def delete(self, request, pk, format=None):

        session = Session.objects.get(
            tAccess=request.META.get('HTTP_AUTHORIZATION')[7:],
            user=request.user)
        name_user = session.user

        try:
            room = Room.objects.get(pk=pk)
        except Room.DoesNotExist:
            raise Http404

        owner = Player.objects.filter(room=room,
                                      isGameOwner=True).first().user

        # Solo el que creo la sala puede borrarla.
        if owner != name_user:
            return Response("No tiene permisos para borrar la sala",
                            status=status.HTTP_401_UNAUTHORIZED)

        # Si el juego está empezado, devuelve un error.
        if Room.objects.filter(pk=pk, game_has_started=True).count() > 0:
            return Response("El juego ya empezo",
                            status=status.HTTP_409_CONFLICT)

        # Guarda el juego al que se asocia esta sala.
        game = room.game

        # Borra ese juego, y por ende, la sala.
        game.delete()
        return Response("Juego borrado", status=status.HTTP_204_NO_CONTENT)
