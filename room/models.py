from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from board.models import Board
from catan_configuration.game_elements import GameElements
from game.models import Game


class Room(models.Model):
    name = models.CharField(max_length=150)
    max_players = models.PositiveSmallIntegerField(
        default=4,
        validators=[MinValueValidator(3), MaxValueValidator(4)]
    )
    state = models.CharField(max_length=12, choices=GameElements.ROOM_STATUS,
                             default='Available')
    game_has_started = models.BooleanField(default=False)
    game = models.ForeignKey(Game, null=True, on_delete=models.CASCADE,
                             related_name='game')

    def __str__(self):
        return self.name
