# Settlers of catan - Los exiliados

---
## Introduccion
Este proyecto es un trabajo de la parte de laboratorio de la materia de
ingenieria del software. En el, nos pusimos como objetivo programar el juego
'settlers of catan' usando un modelo de proceso de software especifico: scrum.

Al trabajar con scrum, decidimos nombrar nuestro 'Los exiliados', el cual esta
conformado por los siguientes integrantes:

- Alex
- Agustin
- Julieta
- Mercedes

---
## Settlers of catan
Originalmente, este es un juego de mesa de administracion de recursos, por
turnos, donde hay un tablero con hexagonos y tokens. Cada hexagono tiene un
recurso que produce cuando con los dados sale el token que contiene el mismo
hexagono.

Settlers of catan puede jugarse tanto de a tres jugadores como de a cuatro. La
esencia del juego es construir asentamientos y carreteras en los bordes y en las
esquinas de los hexagonos respectivamente.

Una mecanica interesante del juego es que se puede comerciar (tradear) recursos
con el banco, con los puertos y entre jugadores. Tambien hay cartas de
desarrollo, que pueden cambiar las reglas del juego con su uso. 

Cuando los dados marcan 7, entra en juego un ente llamado ladron, que anula la
produccion de recursos del hexagono en el que se coloca.

El ganador del juego es aquel que primero sume 10 puntos.

---
## Descripcion del proyecto
Nosotros nos encargaremos de ciertas funcionalidades del juego (no todas) y solo
implementaremos la parte de backend. Vamos a trabajar siguiendo una
especificacion hecha por los profes, la cual describe una
[API](https://docs.google.com/spreadsheets/d/10tRfyxZQ1K853KEcaBvR25k9qo9sGdnkFSxU45IyS6Y/edit#gid=1282872825)

Vamos a crear algunos tableros prediseñados, recrear salas de cuatro jugadores
y toda la mecanica de las acciones.

Decidimos organizar el proyecto clasificando los archivos por modulo, por lo que
no vamos a tener un main que integre todas las funcionalidades de la API. Cada
modulo tiene un modelo, un serializador, un conjunto de urls, un set de vistas
y un conjunto de test.
De esta manera es mas facil trabajar por separado, hacer branches y mergear.
Tambien podemos importar facilmente componentes de otro modulo, de la
siguiente forma:

```python
from nombredelmodulo.archivo import ClaseQueNecesitamos
```

---
## Guia de instalacion
Para poder tener este proyecto seguir los siguientes pasos:

1. Instalar python >= 3.5: `sudo apt install python3.x`

1. Crear una carpeta contenedora para el proyecto y el entorno virtual:
`mkdir Proyecto_LosExiliados`
`cd Proyecto_LosExiliados`

1. Crear un entorno virtual llamado EntornoVirtual donde instalaremos
librerias que necesitamos y correremos el proyecto:
`python3.x -m venv EntornoVirtual`

1. Entrar al entorno virtual: `source EntornoVirtual/bin/activate`
(Nota: Para salir del entorno virtual: `deactivate`

1. Instalar Django, RestFramework y SimpleJWT: `pip install Django`
`pip install djangorestframework`
`pip install djangorestframework_simplejwt`
`pip install django-cors-headers`

1. Clonar el proyecto del repositorio: 
`git clone https://gitlab.com/alexWicher/catan_losexiliados.git`

---
## Comandos utiles

* Realizar migraciones: `python manage.py makemigrations`
`python manage.py migrate`

* Correr server: `python manage.py runserver`

* Correr tests: `python manage.py test --tag LIGHT`

* Correr tests con prints para debuggear: `python manage.py test --tag DEBUG`

