from rest_framework.response import Response
from rest_framework.views import APIView

from board.models import Board
from board.serializers import BoardListSerializer


class BoardList(APIView):
    def get(self, request):
        boards = Board.objects.all()
        result = BoardListSerializer(boards, many=True)
        return Response(result.data)
