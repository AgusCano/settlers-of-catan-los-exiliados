import time

from django.contrib.auth import authenticate
from django.test import TestCase
from django.test import tag
from rest_framework.test import APIClient
from rest_framework.utils import json
from rest_framework_simplejwt.tokens import RefreshToken

from board.models import Hex, Board
from game.models import Game
from user.models import User


class getTableroTest(TestCase):
    """
    Para agilizar testeo hay dos clases de test: 'DEBUG' y 'LIGHT'
    - 'LIGHT': Solo hace asserts de los test e imprime si los tests pasan.
    - 'DEBUG': Debug prueba todos los test e imprime en consola lo que los
       test hicieron para mas detalle a la hora de debuguear.
    """

    BOARD_NAME = 'primertestBoard'
    BOARD_NAME2 = 'segundotestBoard'
    GAME_NAME = 'testGame'

    @tag('LIGHT')
    def setUp(self):
        self.user = User.objects.create_user(username='pepe',
                                             password='123qwe')
        self.get_tablero = APIClient().get('')
        self.get_hexs = APIClient().get('')
        self.get_list_boards_endpoint = APIClient().get('')
        self.get_list_boards_count = APIClient().get('')
        self.board = Board.objects.create(name=self.BOARD_NAME)
        self.board2 = Board.objects.create(name=self.BOARD_NAME2)
        self.game = Game.objects.create(name=self.GAME_NAME,
                                        board=self.board)
        self.hex0 = Hex.objects.create(level=0, index=0, terrain='',
                                       token=12, board=self.board)
        self.hex10 = Hex.objects.create(level=1, index=0, terrain='lumber',
                                        token=3, board=self.board)
        self.hex11 = Hex.objects.create(level=1, index=1, terrain='grain',
                                        token=8, board=self.board)
        self.hex12 = Hex.objects.create(level=1, index=2, terrain='ore',
                                        token=11, board=self.board)
        self.hex13 = Hex.objects.create(level=1, index=3, terrain='brick',
                                        token=12, board=self.board)
        self.hex14 = Hex.objects.create(level=1, index=4, terrain='wool',
                                        token=10, board=self.board)
        self.hex15 = Hex.objects.create(level=1, index=5, terrain='wool',
                                        token=6, board=self.board)
        self.client = APIClient()
        user = authenticate(username=self.user.username, password='123qwe')
        accessToken = (RefreshToken.for_user(user)).access_token
        self.client.credentials(
            HTTP_AUTHORIZATION='Bearer ' + str(accessToken))

    @tag('LIGHT')
    def test_get_tablero(self):
        response = self.client.get('/games/' + str(self.game.id) + '/board/')
        assert response.status_code == 200
        self.get_tablero = response
        Board.objects.get(name=self.BOARD_NAME)
        print('\033[32m', "Test tablero (1/4) --> get_tablero: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_get_hexs(self):
        response = self.client.get('/games/' + str(self.game.id) + '/board/')
        assert response.status_code == 200

        responseJson = response.json()
        self.get_hexs = responseJson['hexes']
        for hex in responseJson['hexes']:
            terrainV = hex['terrain']
            tokenV = hex['token']
            posV = (hex['position']['level'], hex['position']['index'])
            hexResponse = Hex.objects.get(terrain=terrainV, token=tokenV,
                                          level=posV[0], index=posV[1])
            assert (hexResponse.board.name == self.BOARD_NAME and
                    hexResponse.token == tokenV and
                    (hexResponse.level, hexResponse.index) == posV and
                    hexResponse.terrain == terrainV)
        print('\033[32m', "Test tablero (2/4) --> get_hexs: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_get_list_boards_endpoint(self):
        response = self.client.get('/boards/')
        assert response.status_code == 200
        self.get_list_boards_endpoint = response
        print('\033[32m',
              "Test tablero (3/4) --> get_list_boards_endpoint: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_get_list_boards_count(self):
        response = self.client.get('/boards/')
        self.assertEqual(2, len(response.data))
        self.get_list_boards_count = response
        print('\033[32m', "Test tablero (4/4) --> get_list_boards_count: OK",
              '\033[0m', sep='')

    @tag('DEBUG')
    def test_debug_model(self):
        self.test_get_tablero()
        self.test_get_hexs()
        self.test_get_list_boards_endpoint()
        self.test_get_list_boards_count()
        print(json.dumps(self.get_list_boards_endpoint.json(), indent=2))
        print(json.dumps(self.get_tablero.json(), indent=2))
        for i in range(0, 7):
            print(json.dumps(self.get_hexs[i], indent=2))
        time.sleep(0.500)  # para que se complete el print
