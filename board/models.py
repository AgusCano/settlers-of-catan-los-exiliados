from django.db import models

from catan_configuration.game_elements import GameElements


class Board(models.Model):
    name = models.CharField(max_length=100)


class Hex(models.Model):
    level = models.IntegerField()
    index = models.IntegerField()
    terrain = models.CharField(max_length=1, choices=GameElements.TERRAIN,
                               default='lumber')
    token = models.IntegerField()
    board = models.ForeignKey(Board, on_delete=models.CASCADE,
                              related_name='hexes')
