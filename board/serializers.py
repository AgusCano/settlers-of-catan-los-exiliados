from rest_framework import serializers

from board.models import Board, Hex


class HexSerializer(serializers.ModelSerializer):
    position = serializers.SerializerMethodField('get_position')

    def get_position(self, obj):
        position = {'level': obj.level, 'index': obj.index}
        return position

    class Meta:
        model = Hex
        fields = ['position', 'terrain', 'token']


class BoardSerializer(serializers.ModelSerializer):
    hexes = HexSerializer(many=True)

    class Meta:
        model = Board
        fields = ['hexes']


class BoardListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Board
        fields = ['id', 'name']
