from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework_simplejwt.serializers import PasswordField
from rest_framework_simplejwt.settings import api_settings
from rest_framework_simplejwt.state import User as sUser
from rest_framework_simplejwt.tokens import RefreshToken

from session.models import Session
from user.models import User


class MyTokenObtainPairSerializer(serializers.Serializer):
    username_field = sUser.USERNAME_FIELD

    default_error_messages = {
        'no_active_account': _(
            'No active account found with the given credentials')
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['user'] = serializers.CharField()
        self.fields['pass'] = PasswordField()

    def validate(self, attrs):
        authenticate_kwargs = {
            self.username_field: attrs['user'],
            'password': attrs['pass'],
        }

        try:
            authenticate_kwargs['request'] = self.context['request']
        except KeyError:
            pass

        self.user = authenticate(**authenticate_kwargs)

        if (self.user == None):
            raise ValidationError('Este usuario no esta registrado')

        refresh = self.get_token(self.user)

        data = {}
        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)

        getUser = User.objects.filter(username=attrs['user']).first()
        Session.objects.create(user=getUser, tAccess=data['access'],
                               tRefresh=data['refresh'])

        return data

    @classmethod
    def get_token(cls, user):
        token = RefreshToken.for_user(user)
        return token


class MyTokenRefreshSerializer(serializers.Serializer):
    refresh = serializers.CharField()

    def validate(self, attrs):
        refresh = RefreshToken(attrs['refresh'])

        data = {'access': str(refresh.access_token)}

        if api_settings.ROTATE_REFRESH_TOKENS:
            if api_settings.BLACKLIST_AFTER_ROTATION:
                try:
                    # Attempt to blacklist the given refresh token
                    refresh.blacklist()
                except AttributeError:
                    # If blacklist app not installed, `blacklist` method will
                    # not be present
                    pass

            refresh.set_jti()
            refresh.set_exp()

            data['refresh'] = str(refresh)

        Session.objects.filter(tRefresh=refresh.token).update(
            tAccess=data['access'])
        return data
