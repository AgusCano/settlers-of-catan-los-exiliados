from django.test import TestCase
from django.test import tag
from rest_framework.test import APIClient

from user.models import User


class TestLogin(TestCase):

    @tag('LIGHT')
    def test_get_login(self):
        User.objects.create_user(username='USERNAME', password='PASSWORD')
        client = APIClient()
        response = client.post('/users/login/', {'user': 'USERNAME',
                                                 'pass': 'PASSWORD'})
        assert response.status_code == 200
        print('\033[32m', "Test login (1/1) --> get_login: OK",
              '\033[0m', sep='')
