from django.db import models

from user.models import User


class Session(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    tAccess = models.CharField(max_length=150)
    tRefresh = models.CharField(max_length=150)
