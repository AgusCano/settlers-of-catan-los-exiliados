import random

from django.db import models
from rest_framework import status
from rest_framework.response import Response

from board.models import Board
from catan_configuration.game_elements import GameElements
from user.models import User


def validateDice(action, game):
    """
    Este metodo devuelve falso si los dados marcan 7. Lo usamos para poder
    validar si una accion esta disponible o no. Si hay un 7 en los dados, la
    unica accion disponible es la de mover ladron.
    """
    if game.currentTurn == 1 or game.currentTurn == 2:
        return True

    else:
        if action == 'move_robber':
            return game.dice1 + game.dice2 == 7

        elif action == 'end_turn' and game.dice1 + game.dice2 == 7:
            return game.robber_moved

        else:
            return game.dice1 + game.dice2 != 7


def availablePayloadRobber(game):
    actual_robber_position = (game.robber_hex_level, game.robber_hex_level)
    result = GameElements.BOARD_MAP

    return [{'position': {'level': i[0], 'index': i[1]},
             'players': [g for g in get_players_hex(game, i[0], i[1])]} for i
            in
            result]


def get_players_hex(game, level, index):
    from construction.models import Settlement
    from cards.models import vertexsOfHex
    vertexList = vertexsOfHex(level, index)
    players = []
    for vertex in vertexList:
        settlement = Settlement.objects.filter(level=vertex[0],
                                               index=vertex[1],
                                               player__game=game,
                                               player__inTurn=False)
        if settlement.count() == 1:
            players.append(settlement.first().player)
    return set([i.user.username for i in players])


def validateTurn(action, game, player):
    """
    Esto para chequear que el juego este en los dos primeros turnos. Si lo
    esta, las unicas acciones disponibles son construir asentamiento y
    construir carretera.
    """
    from construction.models import validateSettlementTurn, validateRoadTurn, \
        Road
    if action == 'build_settlement':
        return validateSettlementTurn(player)

    elif action == 'build_road':
        return validateRoadTurn(player)

    elif action == 'play_road_building_card':
        return not (game.currentTurn == 1 or game.currentTurn == 2)

    elif action == 'end_turn':
        if game.currentTurn == 1:
            return Road.objects.filter(player=player).count() == 1

        elif game.currentTurn == 2:
            return Road.objects.filter(player=player).count() == 2

        else:
            return True

    else:
        return game.currentTurn > 2


def validateLevelIndex(level, index):
    if level == 0 and index != 0:
        return False

    elif (level == 1) and ((index < 0) or (index > 5)):
        return False

    elif (level == 2) and ((index < 0) or (index > 11)):
        return False

    elif (level > 2) or (level < 0):
        return False

    return True


def sameRobberPosition(level, index, levelR, indexR):
    if level == levelR and index == indexR:
        return False
    return True


def moveRobber(game, level, index):
    """
    Cambia la posicion del robber del juego game, al level e index dado.
    """
    if validateLevelIndex(level, index) and sameRobberPosition(level, index,
                                                               game.robber_hex_level,
                                                               game.robber_hex_index):
        game.robber_hex_level = level
        game.robber_hex_index = index
        game.robber_moved = True
        game.save()
        return Response("Ladron movido correctamente",
                        status=status.HTTP_200_OK)
    else:
        return Response("Ladron no movido", status=status.HTTP_400_BAD_REQUEST)


class Game(models.Model):
    name = models.CharField(max_length=50)
    dice1 = models.IntegerField(default=0)
    dice2 = models.IntegerField(default=0)
    robber_hex_level = models.IntegerField(default=-1)
    robber_hex_index = models.IntegerField(default=-1)
    robber_moved = models.BooleanField(default=True)
    board = models.ForeignKey(Board,
                              on_delete=models.CASCADE,
                              related_name='board')
    currentTurn = models.IntegerField(default=0)

    def RollDice(self):
        if self.currentTurn != 1 and self.currentTurn != 2:
            self.dice1 = random.randrange(1, 6)
            self.dice2 = random.randrange(1, 6)
        if self.dice1 + self.dice2 == 7:
            self.robber_moved = False
