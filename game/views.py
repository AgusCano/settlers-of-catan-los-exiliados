from django.http import Http404
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from board.serializers import Board, BoardSerializer
from game.models import Game
from game.serializers import GameSerializer, GameDetailsSerializer
from player.serializers import Player, PlayerSerializer
from session.models import Session


class GameviewSet(viewsets.ModelViewSet):

    @action(methods=['get'], detail=True)
    def board(self, request, pk):
        try:
            game = Game.objects.get(id=pk)
            board = Board.objects.get(id=game.board_id)
            serializer = BoardSerializer(board)
            return Response(serializer.data)
        except Game.DoesNotExist:
            raise Http404

    def list(self, request, pk=None):
        games = Game.objects.filter(player__inTurn=True)
        serializer = GameSerializer(games, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk):
        game = Game.objects.get(id=pk)
        serializer = GameDetailsSerializer(game)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def player(self, request, pk):
        sesion = Session.objects.filter(
            tAccess=request.META.get('HTTP_AUTHORIZATION')[7:]).first()
        if sesion is None: raise ValidationError('No se encuentra la sesion')

        if (pk == 'undefined'):
            return Response(status=status.HTTP_400_BAD_REQUEST)

        try:
            game = Game.objects.get(id=pk)
        except Game.DoesNotExist:
            raise Http404

        try:
            player = Player.objects.get(user=sesion.user, game=game)
        except Player.DoesNotExist:
            raise Http404
        serializer = PlayerSerializer(player)
        return Response(serializer.data)

    def list(self, request, pk=None):
        games = Game.objects.filter(player__inTurn=True)
        serializer = GameSerializer(games, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk):
        try:
            game = Game.objects.get(id=pk)
            serializer = GameDetailsSerializer(game)
            return Response(serializer.data)
        except Game.DoesNotExist:
            raise Http404
