from rest_framework import serializers

from cards.models import Resource, Card
from construction.models import Settlement, Road
from game.models import Game
from player.models import Player


class GameSerializer(serializers.ModelSerializer):
    in_turn = serializers.SerializerMethodField('get_in_turn')

    def get_in_turn(self, obj):
        return Player.objects.get(game=obj, inTurn=True).user.username

    class Meta:
        model = Game
        fields = ('id', 'name', 'in_turn')


def player_to_json(game, p):
    return {
        "username": p.user.username,
        "colour": p.colour,
        "settlements": [{'level': i.level, 'index': i.index} for i in
                        Settlement.objects.filter(player=p, isCity=False)],
        "cities": [{"level": i.level, "index": i.index}
                   for i in
                   Settlement.objects.filter(player=p, isCity=True)],
        "roads": [[{'level': i.vertex_level_A, 'index': i.vertex_index_A},
                   {'level': i.vertex_level_B, 'index': i.vertex_index_B}]
                  for i in Road.objects.filter(player=p)],
        "development_cards": Card.objects.filter(player=p).count(),
        "resources_cards": Resource.objects.filter(player=p).count(),
        "last_gained": [p.lastGained],
        "victory_points": p.victoryPoint
    }


class GameDetailsSerializer(serializers.ModelSerializer):
    players = serializers.SerializerMethodField('get_players')
    robber = serializers.SerializerMethodField('get_robber')
    current_turn = serializers.SerializerMethodField('get_current_turn')
    winner = serializers.SerializerMethodField('get_winner')

    def get_players(self, obj):
        return [player_to_json(obj, i) for i in
                Player.objects.filter(game=obj)]

    def get_robber(self, obj):
        return {'level': obj.robber_hex_level, 'index': obj.robber_hex_index}

    def get_current_turn(self, obj):
        return {
            'user': Player.objects.get(game=obj, inTurn=True).user.username,
            'dice': (obj.dice1, obj.dice2)
            }

    def get_winner(self, obj):
        if Player.objects.filter(game=obj, victoryPoint=10).exists():
            return Player.objects.get(game=obj, victoryPoint=10).user.username
        else:
            return None

    class Meta:
        model = Game
        fields = ('players', 'robber', 'current_turn', 'winner')
