from django.test import TestCase
from django.test import tag

from board.models import Board
from cards.models import Resource, discardCards
from game.models import Game
from player.models import Player
from room.models import Room
from user.models import User


class TestRollDices(TestCase):
    """
    Para agilizar testeo hay dos clases de test: 'DEBUG' y 'LIGHT'
    - 'LIGHT': Solo hace asserts de los test e imprime si los tests pasan.
    - 'DEBUG': Debug prueba todos los test e imprime en consola lo que los
       test hicieron para mas detalle a la hora de debuguear.
    """

    game = None

    @tag('LIGHT')
    def setUp(self):
        board = Board.objects.create(name='board')
        self.game = Game.objects.create(name='game',
                                        currentTurn=1,
                                        board=board)

    @tag('LIGHT')
    def test_roll_dices(self):
        assert self.game.dice1 == 0 and self.game.dice2 == 0
        self.game.currentTurn = 23
        self.game.RollDice()
        assert (self.game.dice1 < 7 and self.game.dice1 > 0
                and self.game.dice2 < 7 and self.game.dice2 > 0)
        print('\033[32m', "Test game (1/2) --> roll_dices: OK",
              '\033[0m', sep='')


class TestDiscardCards(TestCase):
    """
    Para agilizar testeo hay dos clases de test: 'DEBUG' y 'LIGHT'
    - 'LIGHT': Solo hace asserts de los test e imprime si los tests pasan.
    - 'DEBUG': Debug prueba todos los test e imprime en consola lo que los
       test hicieron para mas detalle a la hora de debuguear.
    """

    game = None
    player = None

    @tag('LIGHT')
    def setUp(self):
        board = Board.objects.create(name='board')
        user = User.objects.create_user(username='pepe', password='123qwe')
        room = Room.objects.create(name='room', max_players=4,
                                   state='Available', game_has_started=False)
        self.game = Game.objects.create(name='gm', currentTurn=1, board=board)
        self.player = Player.objects.create(user=user, room=room,
                                            colour='Azul',
                                            inTurn=True, game=self.game)
        for i in range(8):
            Resource.objects.create(resource='wool', player=self.player)

    @tag('LIGHT')
    def test_discard_cards(self):
        self.game.dice1 = 3
        self.game.dice2 = 4
        discardCards(self.game)

        assert Resource.objects.filter(player=self.player).count() == 4
        print('\033[32m', "Test game (2/2) --> discard_card: OK",
              '\033[0m', sep='')
