from django.db import models
from django.db.models import Q
from rest_framework.exceptions import ValidationError

from cards.models import Resource
from catan_configuration.game_elements import GameElements
from player.models import Player

FALSE_IDX = -1

cycles = [[1], [2, 4], [5, 7], [8, 10], [11, 13], [14, 16], [17]]
reverseCycles = [[1], [4, 6], [9, 11], [14, 16], [19, 21], [24, 26], [29]]


def validateVertex(level, index):
    vertex = (level, index)
    return vertex in GameElements.VERTEX_MAP


def validateEdge(levelA, indexA, levelB, indexB):
    vertexA = (levelA, indexA)
    vertexB = (levelB, indexB)

    if (not validateVertex(levelA, indexA) or not validateVertex(levelB,
                                                                 indexB) or
            (levelA == levelB and indexA == indexB)):
        return False

    if levelA == 0:
        return (vertexB == (0, 0 if vertexA[1] == 5 else vertexA[1] + 1) or
                vertexB == (0, 5 if vertexA[1] == 0 else vertexA[1] - 1) or
                vertexB == (1, 3 * vertexA[1]))

    if levelA == 1:
        lowerLevel = 0 if vertexA[1] == 0 else (
            vertexA[1] / 3 if (vertexA[1] <= 15 and vertexA[
                1] % 3 == 0) else FALSE_IDX)
        higherLevel = [i for i, cycle in enumerate(cycles) if
                       vertexA[1] in cycle]
        return (vertexB == (1, 0 if vertexA[1] == 17 else vertexA[1] + 1) or
                vertexB == (1, 17 if vertexA[1] == 0 else vertexA[1] - 1) or
                vertexB == (0, lowerLevel) or
                vertexB == (2, FALSE_IDX if len(higherLevel) != 1 else vertexA[
                                                                           1] + 2 *
                                                                       higherLevel[
                                                                           0])
                )

    if levelA == 2:
        lowerLevel = [i for i, cycle in enumerate(reverseCycles) if
                      vertexA[1] in cycle]

        return (vertexB == (2, 0 if vertexA[1] == 29 else vertexA[1] + 1) or
                vertexB == (2, 29 if vertexA[1] == 0 else vertexA[1] - 1) or
                vertexB == (1, FALSE_IDX if len(lowerLevel) != 1 else vertexA[
                                                                          1] - 2 *
                                                                      lowerLevel[
                                                                          0])
                )


def validateRoadTurn(player):
    return ((player.game.currentTurn == 1 and
             Road.objects.filter(player=player).count() == 0 and
             Settlement.objects.filter(player=player).count() == 1) or
            (player.game.currentTurn == 2 and
             Road.objects.filter(player=player).count() == 1 and
             Settlement.objects.filter(player=player).count() == 2) or
            (player.game.currentTurn > 2))


def validateResourcesRoad(player):
    if player.game.currentTurn > 2:
        return ((Resource.objects.filter(player=player,
                                         resource='brick').count() > 0) and
                (Resource.objects.filter(player=player,
                                         resource='lumber').count() > 0))
    else:
        return True


def validateRoadPosition(road):
    occupied = Road.objects.filter(
        (Q(vertex_level_A=road.vertex_level_A) & Q(
            vertex_index_A=road.vertex_index_A)) &
        (Q(vertex_level_B=road.vertex_level_B) & Q(
            vertex_index_B=road.vertex_index_B)) |
        (Q(vertex_level_B=road.vertex_level_A) & Q(
            vertex_index_B=road.vertex_index_A)) &
        (Q(vertex_level_A=road.vertex_level_B) & Q(
            vertex_index_A=road.vertex_index_B)),
        player__game=road.player.game).count()

    settlement = Settlement.objects.filter(
        (Q(level=road.vertex_level_A) & Q(index=road.vertex_index_A)) |
        (Q(level=road.vertex_level_B) & Q(index=road.vertex_index_B)),
        player=road.player).count()

    roads = Road.objects.filter(
        Q(vertex_level_A=road.vertex_level_A) & Q(
            vertex_index_A=road.vertex_index_A) |
        Q(vertex_level_B=road.vertex_level_B) & Q(
            vertex_index_B=road.vertex_index_B) |
        Q(vertex_level_B=road.vertex_level_A) & Q(
            vertex_index_B=road.vertex_index_A) |
        Q(vertex_level_A=road.vertex_level_B) & Q(
            vertex_index_A=road.vertex_index_B),
        player=road.player).count()

    if settlement == 0 and roads == 0:
        return False
    return (occupied == 0) and (settlement <= 1) and (roads <= 4)


def validateSettlementTurn(player):
    return ((player.game.currentTurn == 1 and
             Settlement.objects.filter(player=player).count() == 0) or
            (player.game.currentTurn == 2 and
             Settlement.objects.filter(player=player).count() == 1) or
            (
                    player.game.currentTurn > 2) and player.inTurn and validateResourcesSettlement)


def validateResourcesSettlement(player):
    if player.game.currentTurn > 2:
        return ((Resource.objects.filter(player=player,
                                         resource='brick').count() > 0) and
                (Resource.objects.filter(player=player,
                                         resource='lumber').count() > 0) and
                (Resource.objects.filter(player=player,
                                         resource='wool').count() > 0) and
                (Resource.objects.filter(player=player,
                                         resource='grain').count() > 0))
    else:
        return True


def validateSettlementPosition(settlement):
    global neighbour1, neighbour2, neighbour3
    vertex = (settlement.level, settlement.index)
    players = Player.objects.filter(game=settlement.player.game)
    if Settlement.objects.filter(level=vertex[0], index=vertex[1],
                                 player__in=players).count() != 0:
        return False

    if ((settlement.player.game.currentTurn != 1) and
        (settlement.player.game.currentTurn != 2)) and (
            Road.objects.filter(
                Q(vertex_level_A=vertex[0]) and Q(vertex_index_A=vertex[1]) |
                Q(vertex_level_B=vertex[0]) and Q(vertex_index_B=vertex[1]),
                player=settlement.player).count() == 0
    ):
        return False

    if vertex[0] == 0:
        neighbour1 = (0, 0 if vertex[1] == 5 else vertex[1] + 1)
        neighbour2 = (0, 5 if vertex[1] == 0 else vertex[1] - 1)
        neighbour3 = (1, vertex[1] * 3)

    if vertex[0] == 1:
        lowerLevel = 0 if vertex[1] == 0 else (vertex[1] / 3 if (
                vertex[1] <= 15 and vertex[1] % 3 == 0) else FALSE_IDX)
        higherLevel = [i for i, cycle in enumerate(cycles) if
                       vertex[1] in cycle]

        neighbour1 = (1, 0 if vertex[1] == 17 else vertex[1] + 1)
        neighbour2 = (1, 17 if vertex[1] == 0 else vertex[1] - 1)
        if lowerLevel != FALSE_IDX:
            neighbour3 = (0, lowerLevel)
        if len(higherLevel) == 1:
            neighbour3 = (2, vertex[1] + 2 * higherLevel[0])

    if vertex[0] == 2:
        lowerLevel = [i for i, cycle in enumerate(reverseCycles) if
                      vertex[1] in cycle]
        neighbour1 = (2, 0 if vertex[1] == 29 else vertex[1] + 1)
        neighbour2 = (2, 29 if vertex[1] == 0 else vertex[1] - 1)
        neighbour3 = (
            1,
            FALSE_IDX if len(lowerLevel) != 1 else vertex[1] - 2 * lowerLevel[
                0])

    if Settlement.objects.filter(level=neighbour1[0], index=neighbour1[1],
                                 player__in=players).count() != 0 or (
            Settlement.objects.filter(level=neighbour2[0], index=neighbour2[1],
                                      player__in=players)).count() != 0:
        return False

    if neighbour3[1] != FALSE_IDX:
        if Settlement.objects.filter(level=neighbour3[0], index=neighbour3[1],
                                     player__in=players).count() != 0:
            return False

    return True


def deleteResource(construction, player):
    if construction == 'settlement':
        Resource.objects.filter(resource='brick',
                                player=player).first().delete()
        Resource.objects.filter(resource='lumber',
                                player=player).first().delete()
        Resource.objects.filter(resource='wool',
                                player=player).first().delete()
        Resource.objects.filter(resource='grain',
                                player=player).first().delete()
    elif construction == 'road':
        Resource.objects.filter(resource='brick',
                                player=player).first().delete()
        Resource.objects.filter(resource='lumber',
                                player=player).first().delete()


def availablePayloadRoad(obj, game):
    roads = Road.objects.filter(player=obj)
    settlements = Settlement.objects.filter(player=obj)
    playerVertexs = listVertexs(roads, settlements)

    rawAdyacentVertexs = []
    for i in playerVertexs:
        for j in getAdyacentVertex(i[0], i[1]):
            rawAdyacentVertexs.append([i, j])

    adyacentVertexs = []
    repeated = []
    for idx, val in enumerate(rawAdyacentVertexs):
        count = 0
        for j in rawAdyacentVertexs:
            if val == j or val == [j[1], j[0]] or [val[1], val[0]] == j or [
                j[1], j[0]] == [val[1], val[0]]:
                count = count + 1
        if val not in repeated:
            adyacentVertexs.append(val)
        if count > 1:
            repeated.append(val)
            repeated.append([val[1], val[0]])

    result = []
    for i in adyacentVertexs:
        road = Road(vertex_level_A=i[0][0], vertex_index_A=i[0][1],
                    vertex_level_B=i[1][0],
                    vertex_index_B=i[1][1], player=obj)
        if validateRoadPosition(road) and not (
                Road.objects.filter(vertex_level_A=i[0][0],
                                    vertex_index_A=i[0][1],
                                    vertex_level_B=i[1][0],
                                    vertex_index_B=i[1][1],
                                    player__game=game).exists()
        ):
            result.append(i)
    return [[{'level': i[0][0], 'index': int(i[0][1])},
             {'level': i[1][0], 'index': int(i[1][1])}] for i in result]


def getAdyacentVertex(level, index):
    vertex = (level, index)

    if level == 0:
        return [(0, 0 if vertex[1] == 5 else vertex[1] + 1),
                (0, 5 if vertex[1] == 0 else vertex[1] - 1),
                (1, 3 * vertex[1])]

    if level == 1:
        lowerLevel = 0 if vertex[1] == 0 else (vertex[1] / 3 if (
                vertex[1] <= 15 and vertex[1] % 3 == 0) else FALSE_IDX)
        higherLevel = [i for i, cycle in enumerate(cycles) if
                       vertex[1] in cycle]

        rawList = [(1, 0 if vertex[1] == 17 else vertex[1] + 1),
                   (1, 17 if vertex[1] == 0 else vertex[1] - 1),
                   (0, lowerLevel),
                   (2, FALSE_IDX if len(higherLevel) != 1 else vertex[1] + 2 *
                                                               higherLevel[0])
                   ]

    if level == 2:
        lowerLevel = [i for i, cycle in enumerate(reverseCycles) if
                      vertex[1] in cycle]

        rawList = [(2, 0 if vertex[1] == 29 else vertex[1] + 1),
                   (2, 29 if vertex[1] == 0 else vertex[1] - 1),
                   (1, FALSE_IDX if len(lowerLevel) != 1 else vertex[1] - 2 *
                                                              lowerLevel[0])
                   ]

    result = [i for i in rawList if i[1] != FALSE_IDX]
    return result


def availablePayloadSettlement(player):
    roads = Road.objects.filter(player=player)
    playerVertexs = listVertexs(roads, [])
    adyacentVertexs = []
    repeated = []
    for idx, val in enumerate(playerVertexs):
        count = 0
        for j in playerVertexs:
            if val == j:
                count = count + 1
        if val not in repeated:
            adyacentVertexs.append(val)
        if count > 1:
            repeated.append(val)

    result = []
    if (player.game.currentTurn == 1 or player.game.currentTurn == 2):
        adyacentVertexs = GameElements.VERTEX_MAP

    for i in adyacentVertexs:
        settlement = Settlement(level=i[0], index=i[1], player=player)
        if validateSettlementPosition(settlement):
            result.append(i)
    return [{'level': (i[0]), 'index': i[1]} for i in result]


def listVertexs(roads, settlements):
    vertexs = []
    for i in roads:
        vertexs.append((i.vertex_level_A, i.vertex_index_A))
        vertexs.append((i.vertex_level_B, i.vertex_index_B))
    for i in settlements:
        vertexs.append((i.level, i.index))
    return vertexs


class Settlement(models.Model):
    level = models.IntegerField()
    index = models.IntegerField()
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    isCity = models.BooleanField(default=False)

    def validateAndSave(self, *args, **kwargs):
        if (self.player.inTurn and
                validateVertex(self.level, self.index) and
                validateSettlementPosition(self)):
            self.player.victoryPoint = self.player.victoryPoint + 1
            self.player.save()
            self.save()

            if self.player.game.currentTurn not in [1, 2]:
                deleteResource('settlement', self.player)
        else:
            raise ValidationError("Invalid settlement")


class Road(models.Model):
    vertex_level_A = models.IntegerField()
    vertex_index_A = models.IntegerField()
    vertex_level_B = models.IntegerField()
    vertex_index_B = models.IntegerField()
    player = models.ForeignKey(Player, on_delete=models.CASCADE)

    def validateAndSave(self, card=False):
        if (self.player.inTurn and
                (validateEdge(self.vertex_level_A, self.vertex_index_A,
                              self.vertex_level_B, self.vertex_index_B)) and
                (validateRoadPosition(self))):
            self.save()
            if self.player.game.currentTurn not in [1, 2] and not card:
                deleteResource('road', self.player)

        else:
            raise ValidationError("Invalid road")
