from django.db import models

from catan_configuration.game_elements import GameElements
from game.models import Game
from room.models import Room
from user.models import User


class Player(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.CASCADE, null=True)
    colour = models.CharField(max_length=1, choices=GameElements.COLOUR)
    game = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)
    inTurn = models.BooleanField(default=False)
    winner = models.BooleanField(default=False)
    isGameOwner = models.BooleanField(default=False)
    victoryPoint = models.IntegerField(default=0)
    lastGained = models.CharField(null=True, max_length=1)

    def __str__(self):
        return self.user
