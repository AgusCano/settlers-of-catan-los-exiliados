from rest_framework.exceptions import ValidationError

from board.models import Hex
from cards.models import Resource
from construction.models import Settlement, validateVertex
from player.models import Player


def getAdjacentSettlementHexes(vertex_level, vertex_index):
    LV2_SMALL_MAP = [[0, 1], [2, 3, 4], [5, 6], [7, 8, 9], [10, 11],
                     [12, 13, 14], [15, 16], [17, 18, 19], [20, 21],
                     [22, 23, 24], [25, 26], [27, 28, 29]]

    if not validateVertex(vertex_level, vertex_index):
        raise ValidationError("Invalid vertex postion!")

    if vertex_level == 0:
        neighbourBIndex = vertex_index - 1 if vertex_index != 0 else 5
        return [(0, 0), (1, neighbourBIndex), (1, vertex_index)]

    if vertex_level == 1:
        if vertex_index % 3 == 0:
            neighbourBIndex = int(vertex_index / 3) - 1 if int(
                vertex_index / 3) != 0 else 5
            return [(2, int(vertex_index / 3) * 2), (1, neighbourBIndex),
                    (1, int(vertex_index / 3))]
        else:
            firstHex = (1, int((vertex_index - 1) / 3)) if (
                                                                   vertex_index - 1) % 3 == 0 else (
                1, int((vertex_index - 2) / 3))
            return [firstHex, (
                2, vertex_index - firstHex[1] if vertex_index != 17 else 0),
                    (2, vertex_index - firstHex[1] - 1)]

    if vertex_level == 2:
        last = False
        subListIndex = 0
        out = False
        for i in LV2_SMALL_MAP:
            if out:
                break
            subListIndex = int(LV2_SMALL_MAP.index(i))
            for j in i:
                last = (i.index(j) + 1) == len(i)
                if vertex_index == j:
                    out = True
                    break

        if not last:
            return [(2, subListIndex)]
        else:
            return [(2, subListIndex),
                    (2, subListIndex + 1 if subListIndex != 11 else 0)]


def generateResources(game):
    players = Player.objects.filter(game=game)
    diceSum = game.dice1 + game.dice2
    for player in players:
        settlements = Settlement.objects.filter(player=player)
        for settlement in settlements:
            hexesPos = getAdjacentSettlementHexes(settlement.level,
                                                  settlement.index)
            for hexPos in hexesPos:
                if (hexPos[0], hexPos[1]) != (
                        game.robber_hex_level, game.robber_hex_index):
                    hex = Hex.objects.get(level=hexPos[0], index=hexPos[1],
                                          board=game.board)
                    if hex.token == diceSum:
                        for i in range(2) if settlement.isCity else range(1):
                            Resource.objects.create(player=player,
                                                    resource=hex.terrain)
                        player.lastGained = \
                            Resource.objects.filter(player=player).order_by(
                                '-id')[
                            :1][0].resource
                        player.save()


def endTurn(player, game):
    """
    El orden de los turnos depende del color del jugador, y es:
    1) Rojo
    2) Azul
    3) Amarillo
    4) Blanco
    """

    players = Player.objects.filter(game=game)
    actualColour = player.colour
    if players.count() == 4:
        if actualColour == 'red':
            nextPlayer = Player.objects.get(game=game, colour='blue')
            nextPlayer.inTurn = True
            nextPlayer.save()
        elif actualColour == 'blue':
            game.currentTurn = game.currentTurn + 1
            game.save()
            nextPlayer = Player.objects.get(game=game, colour='yellow')
            nextPlayer.inTurn = True
            nextPlayer.save()
        elif actualColour == 'yellow':
            nextPlayer = Player.objects.get(game=game, colour='white')
            nextPlayer.inTurn = True
            nextPlayer.save()
        else:
            nextPlayer = Player.objects.get(game=game, colour='red')
            nextPlayer.inTurn = True
            nextPlayer.save()

    else:
        coloursTaken = []
        for i in [0, 1, 2]:
            coloursTaken.append(players[i].colour)
        if actualColour == coloursTaken[0]:
            nextPlayer = Player.objects.filter(game=game,
                                               colour=coloursTaken[1]).first()
            nextPlayer.inTurn = True
            nextPlayer.save()
        elif actualColour == coloursTaken[1]:
            nextPlayer = Player.objects.filter(game=game,
                                               colour=coloursTaken[2]).first()
            nextPlayer.inTurn = True
            nextPlayer.save()
        else:
            nextPlayer = Player.objects.filter(game=game,
                                               colour=coloursTaken[0]).first()
            nextPlayer.inTurn = True
            nextPlayer.save()
            game.currentTurn = game.currentTurn + 1
            game.save()
    player.inTurn = False
    player.save()
    game.RollDice()
    game.save()
    if game.currentTurn != 1 and game.currentTurn != 2:
        generateResources(game)
