from rest_framework import serializers

from cards.models import Card, Resource
from player.models import Player


class PlayerSerializer(serializers.ModelSerializer):
    resources = serializers.SerializerMethodField('get_resources')
    cards = serializers.SerializerMethodField('get_cards')

    def get_resources(self, obj):
        return [i.resource for i in Resource.objects.filter(player=obj)]

    def get_cards(self, obj):
        return [i.card_type for i in Card.objects.filter(player=obj)]

    class Meta:
        model = Player
        fields = ['resources', 'cards']
