from django.test import TestCase

from board.models import Board, Hex
from cards.models import Resource
from catan_configuration.game_elements import GameElements
from construction.models import Settlement
from game.models import Game
from player.models import Player
from player.service import generateResources
from session.models import Session
from user.models import User


class generateResourcesTest(TestCase):

    def setUp(self):
        self.board = Board.objects.create(name='board')

        self.game = Game.objects.create(name='colonos glaciales',
                                        board=self.board, dice1=6, dice2=6)
        self.user = User.objects.create_user(username='user', password='pass')
        self.user1 = User.objects.create_user(username='user1',
                                              password='pass')
        self.user2 = User.objects.create_user(username='user2',
                                              password='pass')
        self.user3 = User.objects.create_user(username='user3',
                                              password='pass')
        self.session = Session.objects.create(user=self.user, tAccess='...',
                                              tRefresh='...')
        self.player1 = Player.objects.create(user=self.user, game=self.game,
                                             colour='Rojo')
        self.player2 = Player.objects.create(user=self.user1, game=self.game,
                                             inTurn=True, colour='Azul')
        self.player3 = Player.objects.create(user=self.user2, game=self.game,
                                             colour='Amarillo')
        self.player4 = Player.objects.create(user=self.user3, game=self.game,
                                             colour='Blanco')
        for hex in GameElements.BOARD_MAP:
            Hex.objects.create(level=hex[0], index=hex[1], terrain='lumber',
                               token=12, board=self.board)
        for vertex in GameElements.VERTEX_MAP:
            Settlement.objects.create(level=vertex[0], index=vertex[1],
                                      player=self.player2)

    def test_generate_resources_all_board(self):
        generateResources(self.player2.game)
        assert Resource.objects.filter(player=self.player2).count() == 114
        print('\033[32m', "Test resources --> generate_resource_all_board: OK",
              '\033[0m', sep='')
