from django.contrib.auth import authenticate
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework.utils import json
from rest_framework_simplejwt.tokens import RefreshToken

from board.models import Board
from cards.models import Resource, Card
from construction.models import Settlement, Road
from game.models import Game, moveRobber
from player.models import Player
from player.service import endTurn
from session.models import Session
from user.models import User


class moveRobberTest(TestCase):

    def setUp(self):
        self.board = Board.objects.create(name='board')
        self.game = Game.objects.create(name='colonos glaciales',
                                        board=self.board, currentTurn=69,
                                        dice1=3, dice2=4, robber_moved=False)
        self.user = User.objects.create_user(username='user', password='pass')
        self.user1 = User.objects.create_user(username='user1',
                                              password='pass')
        self.user2 = User.objects.create_user(username='user2',
                                              password='pass')
        self.user3 = User.objects.create_user(username='user3',
                                              password='pass')
        self.session = Session.objects.create(user=self.user, tAccess='...',
                                              tRefresh='...')
        self.player1 = Player.objects.create(user=self.user, game=self.game,
                                             inTurn=True, colour='red')
        self.player2 = Player.objects.create(user=self.user1, game=self.game,
                                             inTurn=False, colour='blue')
        self.player3 = Player.objects.create(user=self.user2, game=self.game,
                                             inTurn=False, colour='yellow')
        self.player4 = Player.objects.create(user=self.user3, game=self.game,
                                             inTurn=False, colour='white')
        self.client = APIClient()

    def test_post_action_move_robber(self):
        client = APIClient()
        user = authenticate(username=self.user.username, password='pass')
        accessToken = (RefreshToken.for_user(user)).access_token

        client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(accessToken))
        response = client.post('/games/' + str(self.game.id) + '/player/'
                               + 'actions/', {'type': 'move_robber',
                                              'payload': {
                                                  "position": {
                                                      "level": 2,
                                                      "index": 3},
                                                  "player": ""}},
                               format='json')
        assert response.status_code == 202
        print('\033[32m', "Test actions (1/37) --> post_move_robber: OK",
              '\033[0m', sep='')

    def test_robber_moved(self):
        self.game.robber_hex_level = 1
        self.game.robber_hex_index = 1
        moveRobber(self.game, 2, 3)
        assert ((self.game.robber_hex_level == 2) and
                (self.game.robber_hex_index == 3))
        print('\033[32m', "Test actions (2/37) --> robber_moved: OK",
              '\033[0m', sep='')

    def test_robber_not_moved(self):
        self.game.robber_hex_level = 2
        self.game.robber_hex_index = 3
        moveRobber(self.game, 2, 3)
        assert ((self.game.robber_hex_level == 2) and
                (self.game.robber_hex_index == 3))
        print('\033[32m', "Test actions (3/37) --> robber_not_moved: OK",
              '\033[0m', sep='')


class endingTurn(TestCase):

    def setUp(self):
        self.board = Board.objects.create(name='board')
        self.game = Game.objects.create(name='colonos glaciales',
                                        board=self.board, currentTurn=3)
        self.user = User.objects.create_user(username='user', password='pass')
        self.user1 = User.objects.create_user(username='user1',
                                              password='pass')
        self.user2 = User.objects.create_user(username='user2',
                                              password='pass')
        self.user3 = User.objects.create_user(username='user3',
                                              password='pass')
        self.session = Session.objects.create(user=self.user, tAccess='...',
                                              tRefresh='...')
        self.player1 = Player.objects.create(user=self.user, game=self.game,
                                             inTurn=True, colour='red')
        self.player2 = Player.objects.create(user=self.user1, game=self.game,
                                             inTurn=False, colour='blue')
        self.player3 = Player.objects.create(user=self.user2, game=self.game,
                                             inTurn=False, colour='yellow')
        self.player4 = Player.objects.create(user=self.user3, game=self.game,
                                             inTurn=False, colour='white')
        self.client = APIClient()

    def test_post_action_end_turn(self):
        client = APIClient()
        user = authenticate(username=self.user.username, password='pass')
        accessToken = (RefreshToken.for_user(user)).access_token
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(accessToken))
        data = json.loads('{"type": "end_turn","payload":""}')

        response = client.post(
            '/games/' + str(self.game.id) + '/player/actions/', data,
            format='json')
        assert response.status_code == 202
        print('\033[32m', "Test actions (4/37) --> post_end_turn: OK",
              '\033[0m', sep='')

    def test_ending_turn(self):
        endTurn(self.player1, self.game)
        assert (self.player1.inTurn == False)
        print('\033[32m', "Test actions (5/37) --> ending_turn: OK",
              '\033[0m', sep='')

    def test_next_player_turn(self):
        endTurn(self.player1, self.game)
        self.player2.refresh_from_db()
        assert self.player2.inTurn == True
        print('\033[32m', "Test actions (6/37) --> next_player_turn: OK",
              '\033[0m', sep='')

    def test_other_player_not_in_turn(self):
        endTurn(self.player1, self.game)
        assert (self.player3.inTurn == False and self.player4.inTurn == False)
        print('\033[32m', "Test actions (7/37) --> other_players_not_in"
                          "_turn: OK", '\033[0m', sep='')


class buildingTest(TestCase):

    def setUp(self):
        self.board = Board.objects.create(name='board')
        self.user = User.objects.create_user(username='pepe',
                                             password='123qwe')
        self.user1 = User.objects.create_user(username='pepe1',
                                              password='123qwe')
        self.user2 = User.objects.create_user(username='pepe2',
                                              password='123qwe')
        self.user3 = User.objects.create_user(username='pepe3',
                                              password='123qwe')
        self.game = Game.objects.create(name='infiernoColono', currentTurn=1,
                                        board=self.board)
        self.session = Session.objects.create(user=self.user,
                                              tAccess='AutenticateBaboso',
                                              tRefresh='...')
        self.player1 = Player.objects.create(user=self.user, game=self.game,
                                             inTurn=True)
        self.player2 = Player.objects.create(user=self.user1, game=self.game,
                                             inTurn=False)
        self.player3 = Player.objects.create(user=self.user2, game=self.game,
                                             inTurn=False)
        self.player4 = Player.objects.create(user=self.user3, game=self.game,
                                             inTurn=False)

        self.brick1 = Resource.objects.create(player=self.player1,
                                              resource='brick')
        self.brick2 = Resource.objects.create(player=self.player1,
                                              resource='brick')
        self.lumber = Resource.objects.create(player=self.player1,
                                              resource='lumber')
        self.lumber2 = Resource.objects.create(player=self.player1,
                                               resource='lumber')
        self.wool = Resource.objects.create(player=self.player1,
                                            resource='wool')
        self.grain = Resource.objects.create(player=self.player1,
                                             resource='grain')

    def test_firstSecondTurnTest(self):
        client = APIClient()

        user = authenticate(username=self.user.username, password='123qwe')
        accessToken = (RefreshToken.for_user(user)).access_token
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(accessToken))

        data = json.loads(
            '{"type": "build_settlement", "payload": {"level": 1, "index": 3}}')

        response = client.post(
            '/games/' + str(self.game.id) + '/player/actions/', data,
            format='json')

        assert response.status_code == 202
        print('\033[32m',
              "Test actions (7/37) --> endpoint_build_settlement_first_turn: OK",
              '\033[0m', sep='')

        settlement = Settlement.objects.get(player=self.player1, index=3,
                                            level=1)
        assert settlement.player == self.player1
        assert settlement.level == 1
        assert settlement.index == 3
        print('\033[32m',
              "Test actions (8/37) --> first_turn_build_settlement: OK",
              '\033[0m', sep='')

        data = json.loads(
            '{"type": "build_road", "payload": [{"level": 1, "index": 3},{"level": 0, "index": 1} ] }')
        response = client.post(
            '/games/' + str(self.game.id) + '/player/actions/', data,
            format='json')
        assert response.status_code == 202
        print('\033[32m',
              "Test actions (9/37) --> endpoint_build_road_first_turn: OK",
              '\033[0m', sep='')

        road = Road.objects.get(player=self.player1, vertex_index_A=3,
                                vertex_index_B=1, vertex_level_A=1,
                                vertex_level_B=0)
        assert road.player == self.player1
        assert (road.vertex_level_A, road.vertex_index_A) == (1, 3)
        assert (road.vertex_level_B, road.vertex_index_B) == (0, 1)
        print('\033[32m', "Test actions (10/37) --> first_turn_build_road: OK",
              '\033[0m', sep='')

        Game.objects.filter(id=self.game.id).update(currentTurn=2)
        self.game = Game.objects.get(id=self.game.id)
        data = json.loads(
            '{"type": "build_settlement", "payload": {"level": 2, "index": 21}}')

        response = client.post(
            '/games/' + str(self.game.id) + '/player/actions/', data,
            format='json')

        assert response.status_code == 202
        print('\033[32m',
              "Test actions (11/37) --> endpoint_second_turn_build_settlement: OK",
              '\033[0m', sep='')

        settlement = Settlement.objects.get(player=self.player1, level=2,
                                            index=21)
        assert settlement.player == self.player1
        assert settlement.level == 2
        assert settlement.index == 21
        print('\033[32m',
              "Test actions (12/37) --> second_turn_build_settlement: OK",
              '\033[0m', sep='')

        data = json.loads(
            '{"type": "build_road", "payload": [{"level": 2, "index": 21},{"level": 1, "index": 13} ] }')
        response = client.post(
            '/games/' + str(self.game.id) + '/player/actions/', data,
            format='json')
        assert response.status_code == 202
        print('\033[32m',
              "Test actions (13/37) --> endpoint_second_turn_build_road: OK",
              '\033[0m', sep='')

        road = Road.objects.get(player=self.player1, vertex_index_A=21,
                                vertex_level_A=2, vertex_level_B=1,
                                vertex_index_B=13)
        assert road.player == self.player1
        assert (road.vertex_level_A, road.vertex_index_A) == (2, 21)
        assert (road.vertex_level_B, road.vertex_index_B) == (1, 13)
        print('\033[32m',
              "Test actions (14/37) --> second_turn_build_road: OK",
              '\033[0m', sep='')

    def test_genericTurnTest(self):
        self.settlement1 = Settlement.objects.create(player=self.player1,
                                                     level=2, index=23)
        self.settlement2 = Settlement.objects.create(player=self.player1,
                                                     level=2, index=25)
        self.road1 = Road.objects.create(player=self.player1, vertex_level_A=2,
                                         vertex_level_B=2, vertex_index_A=23,
                                         vertex_index_B=24)
        self.road2 = Road.objects.create(player=self.player1, vertex_level_A=2,
                                         vertex_level_B=2, vertex_index_A=25,
                                         vertex_index_B=24)
        self.road3 = Road.objects.create(player=self.player1, vertex_level_A=2,
                                         vertex_level_B=1, vertex_index_A=24,
                                         vertex_index_B=14)
        self.road4 = Road.objects.create(player=self.player1, vertex_level_A=1,
                                         vertex_level_B=1, vertex_index_A=14,
                                         vertex_index_B=15)
        self.game.currentTurn = 23
        self.game.save()

        client = APIClient()

        user = authenticate(username=self.user.username, password='123qwe')
        accessToken = (RefreshToken.for_user(user)).access_token
        Player.objects.filter(id=self.player1.id).update(user=user)
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(accessToken))

        data = json.loads(
            '{"type": "build_settlement", "payload": {"level": 1, "index": 15} }')

        response = client.post(
            '/games/' + str(self.game.id) + '/player/actions/', data,
            format='json')

        assert response.status_code == 202
        print('\033[32m',
              "Test actions (15/37) --> endpoint_build_settlement: OK",
              '\033[0m', sep='')

        settlement = Settlement.objects.get(player=self.player1, level=1,
                                            index=15)
        assert settlement.player == self.player1
        assert settlement.level == 1
        assert settlement.index == 15
        print('\033[32m', "Test actions (16/37) --> build_settlement: OK",
              '\033[0m', sep='')

        data = json.loads(
            '{"type": "build_road", "payload": [{"level": 1, "index": 15},{"level": 0, "index": 5} ] }')
        response = client.post(
            '/games/' + str(self.game.id) + '/player/actions/', data,
            format='json')
        assert response.status_code == 202
        print('\033[32m', "Test actions (17/37) --> endpoint_build_road: OK",
              '\033[0m', sep='')

        road = Road.objects.get(player=self.player1, vertex_level_A=1,
                                vertex_level_B=0, vertex_index_A=15,
                                vertex_index_B=5)
        assert road.player == self.player1
        assert (road.vertex_level_A, road.vertex_index_A) == (1, 15)
        assert (road.vertex_level_B, road.vertex_index_B) == (0, 5)
        print('\033[32m', "Test actions (18/37) --> build_road: OK", '\033[0m',
              sep='')

    def test_win_game(self):
        self.settlement1 = Settlement.objects.create(player=self.player1,
                                                     level=2, index=23)
        self.settlement2 = Settlement.objects.create(player=self.player1,
                                                     level=2, index=25)
        self.road1 = Road.objects.create(player=self.player1, vertex_level_A=2,
                                         vertex_level_B=2, vertex_index_A=23,
                                         vertex_index_B=24)
        self.road2 = Road.objects.create(player=self.player1, vertex_level_A=2,
                                         vertex_level_B=2, vertex_index_A=25,
                                         vertex_index_B=24)
        self.road3 = Road.objects.create(player=self.player1, vertex_level_A=2,
                                         vertex_level_B=1, vertex_index_A=24,
                                         vertex_index_B=14)
        self.road4 = Road.objects.create(player=self.player1, vertex_level_A=1,
                                         vertex_level_B=1, vertex_index_A=14,
                                         vertex_index_B=15)
        self.player1.victoryPoint = 10
        self.player1.save()
        self.game.currentTurn = 23
        self.game.save()

        client = APIClient()

        user = authenticate(username=self.user.username, password='123qwe')
        accessToken = (RefreshToken.for_user(user)).access_token
        Player.objects.filter(id=self.player1.id).update(user=user)
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(accessToken))

        data = json.loads(
            '{"type": "build_settlement", "payload": {"level": 1, "index": 15} }')

        response = client.get(
            '/games/' + str(self.game.id) + '/player/actions/', data,
            format='json')

        assert response.status_code == 200
        print('\033[32m', "Test actions (19/37) --> win_game: OK", '\033[0m',
              sep='')


class tradeWithBank(TestCase):

    def setUp(self):
        self.board = Board.objects.create(name='board')
        self.user = User.objects.create_user(username='pepe',
                                             password='123qwe')
        self.user1 = User.objects.create_user(username='pepe1',
                                              password='123qwe')
        self.user2 = User.objects.create_user(username='pepe2',
                                              password='123qwe')
        self.user3 = User.objects.create_user(username='pepe3',
                                              password='123qwe')
        self.game = Game.objects.create(name='infiernoColono', currentTurn=4,
                                        board=self.board)
        self.session = Session.objects.create(user=self.user,
                                              tAccess='AutenticateBaboso',
                                              tRefresh='...')
        self.player1 = Player.objects.create(user=self.user, game=self.game,
                                             inTurn=True)
        self.player2 = Player.objects.create(user=self.user1, game=self.game,
                                             inTurn=False)
        self.player3 = Player.objects.create(user=self.user2, game=self.game,
                                             inTurn=False)
        self.player4 = Player.objects.create(user=self.user3, game=self.game,
                                             inTurn=False)

        self.brick1 = Resource.objects.create(player=self.player1,
                                              resource='brick')
        self.brick2 = Resource.objects.create(player=self.player1,
                                              resource='brick')
        self.brick3 = Resource.objects.create(player=self.player1,
                                              resource='brick')
        self.brick4 = Resource.objects.create(player=self.player1,
                                              resource='brick')
        self.brick5 = Resource.objects.create(player=self.player1,
                                              resource='brick')
        self.brick6 = Resource.objects.create(player=self.player1,
                                              resource='brick')

    def test_tradeWithBank(self):
        brickCount = Resource.objects.filter(player=self.player1,
                                             resource='brick').count()
        oreCount = Resource.objects.filter(player=self.player1,
                                           resource='ore').count()
        client = APIClient()

        user = authenticate(username=self.user.username, password='123qwe')
        accessToken = (RefreshToken.for_user(user)).access_token
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(accessToken))

        data = json.loads(
            '{"type": "bank_trade", "payload": {"give": "brick", "receive": "ore"} }')

        response = client.post(
            '/games/' + str(self.game.id) + '/player/actions/', data,
            format='json')

        assert response.status_code == 202
        print('\033[32m', "Test actions (20/37) --> endpoint_trade_bank: OK",
              '\033[0m', sep='')

        assert Resource.objects.filter(player=self.player1,
                                       resource='brick').count() == brickCount - 4 and (
                       Resource.objects.filter(player=self.player1,
                                               resource='ore').count() == oreCount + 1
               )
        oreCount += 1
        brickCount = brickCount - 4
        print('\033[32m', "Test actions (21/37) --> trade_successful: OK",
              '\033[0m', sep='')

        response = client.post(
            '/games/' + str(self.game.id) + '/player/actions/', data,
            format='json')

        assert response.status_code == 400
        print('\033[32m', "Test actions (22/37) --> trade_failed: OK",
              '\033[0m', sep='')

        assert Resource.objects.filter(player=self.player1,
                                       resource='brick').count() == brickCount and (
                       Resource.objects.filter(player=self.player1,
                                               resource='ore').count() == oreCount
               )
        print('\033[32m',
              "Test actions (23/37) --> trade_successful_not_enough_resources: OK",
              '\033[0m', sep='')


class cardTest(TestCase):

    def setUp(self):
        self.board = Board.objects.create(name='board')
        self.user = User.objects.create_user(username='pepe',
                                             password='123qwe')
        self.user1 = User.objects.create_user(username='pepe1',
                                              password='123qwe')
        self.user2 = User.objects.create_user(username='pepe2',
                                              password='123qwe')
        self.user3 = User.objects.create_user(username='pepe3',
                                              password='123qwe')
        self.game = Game.objects.create(name='infiernoColono', currentTurn=3,
                                        board=self.board)
        self.session = Session.objects.create(user=self.user,
                                              tAccess='AutenticateBaboso',
                                              tRefresh='...')
        self.player1 = Player.objects.create(user=self.user, game=self.game,
                                             inTurn=True)
        self.player2 = Player.objects.create(user=self.user1, game=self.game,
                                             inTurn=False)
        self.player3 = Player.objects.create(user=self.user2, game=self.game,
                                             inTurn=False)
        self.player4 = Player.objects.create(user=self.user3, game=self.game,
                                             inTurn=False)

        self.grain1 = Resource.objects.create(player=self.player1,
                                              resource='grain')
        self.wool1 = Resource.objects.create(player=self.player1,
                                             resource='wool')
        self.ore1 = Resource.objects.create(player=self.player1,
                                            resource='ore')
        self.grain2 = Resource.objects.create(player=self.player1,
                                              resource='grain')
        self.wool2 = Resource.objects.create(player=self.player1,
                                             resource='wool')
        self.ore2 = Resource.objects.create(player=self.player1,
                                            resource='ore')

    def test_getCards(self):
        cardsCount = Card.objects.filter(player=self.player1).count()
        client = APIClient()

        user = authenticate(username=self.user.username, password='123qwe')
        accessToken = (RefreshToken.for_user(user)).access_token
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(accessToken))

        data = json.loads('{"type": "buy_card", "payload": {}}')

        response = client.post(
            '/games/' + str(self.game.id) + '/player/actions/', data,
            format='json')
        assert response.status_code == 202
        print('\033[32m', "Test actions (24/37) --> buy_first_card: OK",
              '\033[0m', sep='')

        response = client.post(
            '/games/' + str(self.game.id) + '/player/actions/', data,
            format='json')
        assert response.status_code == 202
        print('\033[32m', "Test actions (25/37) --> buy_second_card: OK",
              '\033[0m', sep='')

        response = client.post(
            '/games/' + str(self.game.id) + '/player/actions/', data,
            format='json')
        assert response.status_code == 400
        print('\033[32m', "Test actions (26/37) --> failed_buy_card: OK",
              '\033[0m', sep='')

        assert cardsCount == Card.objects.filter(
            player=self.player1).count() - 2


class KnightCard(TestCase):

    def setUp(self):
        self.board = Board.objects.create(name='board')
        self.game = Game.objects.create(name='colonos glaciales',
                                        board=self.board, currentTurn=4,
                                        dice1=2, dice2=3)
        self.user = User.objects.create_user(username='hercules',
                                             password='123qwe')
        self.user2 = User.objects.create_user(username='toshi',
                                              password='123qwe')
        self.user3 = User.objects.create_user(username='toshi1',
                                              password='123qwe')
        self.user4 = User.objects.create_user(username='toshi2',
                                              password='123qwe')

        self.session = Session.objects.create(user=self.user, tAccess='...',
                                              tRefresh='...')

        self.player3 = Player.objects.create(user=self.user, game=self.game,
                                             inTurn=False, colour='yellow')
        self.player4 = Player.objects.create(user=self.user2, game=self.game,
                                             inTurn=False, colour='white')

        self.user2 = authenticate(username=self.user3.username,
                                  password='123qwe')
        accessToken2 = (RefreshToken.for_user(self.user3)).access_token
        self.player2 = Player.objects.create(user=self.user3, game=self.game,
                                             inTurn=False, colour='blue')
        Resource.objects.create(player=self.player2, resource='lumber')
        Resource.objects.create(player=self.player2, resource='lumber')
        Settlement.objects.create(player=self.player2, level=2, index=6)

        self.client = APIClient()
        self.user = authenticate(username=self.user4.username,
                                 password='123qwe')
        accessToken = (RefreshToken.for_user(self.user4)).access_token
        self.session = Session.objects.create(
            tRefresh=(RefreshToken.for_user(self.user)), tAccess=accessToken,
            user=self.user)
        self.token = str(accessToken)
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        self.player1 = Player.objects.create(user=self.user4, game=self.game,
                                             inTurn=True, colour='red')
        Card.objects.create(player=self.player1, card_type='knight')

    def test_play_knight_card_endpoint(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(self.token))

        data = json.loads(
            '{"type": "play_knight_card", "payload": {"position": {"level": 2,'
            ' "index": 2} ,"player": ""} }')
        response = self.client.post('/games/' + str(self.game.id) + '/player/'
                                    + 'actions/', data, format='json')

        assert response.status_code == 202
        print('\033[32m', "Test actions (27/37) --> knight_card_endpoint: OK",
              '\033[0m', sep='')

    def test_play_knight_card_steal_cards(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(self.token))

        data = json.loads(
            '{"type": "play_knight_card", "payload": {"position": {"level": 2,'
            ' "index": 2} ,"player": ""} }')
        response = self.client.post('/games/' + str(self.game.id) + '/player/'
                                    + 'actions/', data, format='json')

        assert ((Resource.objects.filter(player=self.player2).count() == 1) and
                (Resource.objects.filter(player=self.player1).count() == 1))

        print('\033[32m',
              "Test actions (28/37) --> knight_card_steal_cards: OK",
              '\033[0m', sep='')

    def test_valid_position(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(self.token))

        assert ((self.game.robber_hex_level == -1) and
                (self.game.robber_hex_index == -1))

        data = json.loads(
            '{"type": "play_knight_card", "payload": {"position": {"level": 2,'
            ' "index": 2} ,"player": ""} }')
        response = self.client.post('/games/' + str(self.game.id) + '/player/'
                                    + 'actions/', data, format='json')

        assert ((Game.objects.get(id=self.game.id).robber_hex_level == 2) and
                (Game.objects.get(id=self.game.id).robber_hex_index == 2))
        print('\033[32m',
              "Test actions (29/37) --> knight_card_valid_position: OK",
              '\033[0m', sep='')

    def test_play_knight_card_delete_card(self):
        # La carta existe antes de jugarla.
        assert Card.objects.filter(player=self.player1,
                                   card_type='knight').exists()

        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(self.token))

        data = json.loads(
            '{"type": "play_knight_card", "payload": {"position": {"level": 1,'
            ' "index": 4} ,"player": ""} }')
        response = self.client.post('/games/' + str(self.game.id) + '/player/'
                                    + 'actions/', data, format='json')
        # La carta ya no existe despues de jugarla.
        assert not Card.objects.filter(player=self.player1,
                                       card_type='knight').exists()
        print('\033[32m', "Test actions (30/37) --> knight_card_delete: OK",
              '\033[0m', sep='')


class playCardBuildingTest(TestCase):

    def setUp(self):
        self.board = Board.objects.create(name='board')
        self.user = User.objects.create_user(username='pepe',
                                             password='123qwe')
        self.user1 = User.objects.create_user(username='pepe1',
                                              password='123qwe')
        self.user2 = User.objects.create_user(username='pepe2',
                                              password='123qwe')
        self.user3 = User.objects.create_user(username='pepe3',
                                              password='123qwe')
        self.game = Game.objects.create(name='infiernoColono', currentTurn=23,
                                        board=self.board)
        self.session = Session.objects.create(user=self.user,
                                              tAccess='AutenticateBaboso',
                                              tRefresh='...')
        self.player1 = Player.objects.create(user=self.user, game=self.game,
                                             inTurn=True)
        self.player2 = Player.objects.create(user=self.user1, game=self.game,
                                             inTurn=False)
        self.player3 = Player.objects.create(user=self.user2, game=self.game,
                                             inTurn=False)
        self.player4 = Player.objects.create(user=self.user3, game=self.game,
                                             inTurn=False)
        self.card = Card.objects.create(player=self.player1,
                                        card_type='road_building')
        self.settlement = Settlement.objects.create(player=self.player1,
                                                    level=0, index=0)
        self.client = APIClient()
        self.user = authenticate(username=self.user.username,
                                 password='123qwe')
        self.token = (RefreshToken.for_user(self.user)).access_token

    def test_play_road_building_card(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(self.token))

        data = json.loads(
            '{"type": "play_road_building_card", "payload": ['
            '[{"level": 0, "index": 0},{"level": 0, "index": 1} ],'
            '[{"level": 0, "index": 1},{"level": 0, "index": 2} ]'
            '] }')
        response = self.client.post('/games/' + str(self.game.id) + '/player/'
                                    + 'actions/', data, format='json')
        assert response.status_code == 202
        assert Road.objects.filter(player=self.player1, vertex_index_A=0,
                                   vertex_level_A=0, vertex_index_B=1,
                                   vertex_level_B=0).exists()
        assert Road.objects.filter(player=self.player1, vertex_index_A=1,
                                   vertex_level_A=0, vertex_index_B=2,
                                   vertex_level_B=0).exists()
        print('\033[32m',
              "Test actions (31/37) --> road_building_card_valid: OK",
              '\033[0m', sep='')

    def test_play_road_building_card_bad_position(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(self.token))

        data = json.loads(
            '{"type": "play_road_building_card", "payload": ['
            '[{"level": 0, "index": 0},{"level": 0, "index": 1} ],'
            '[{"level": 0, "index": 2},{"level": 0, "index": 3} ]'
            '] }')
        response = self.client.post('/games/' + str(self.game.id) + '/player/'
                                    + 'actions/', data, format='json')
        assert response.status_code == 400
        assert Road.objects.filter(player=self.player1, vertex_index_A=0,
                                   vertex_level_A=0, vertex_index_B=1,
                                   vertex_level_B=0).exists()
        assert not Road.objects.filter(player=self.player1, vertex_index_A=2,
                                       vertex_level_A=0, vertex_index_B=3,
                                       vertex_level_B=0).exists()
        print('\033[32m',
              "Test actions (32/37) --> play_road_building_card_bad_position: OK",
              '\033[0m', sep='')

    def test_play_road_building_card_bad_turn(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(self.token))
        Game.objects.filter(id=self.game.id).update(currentTurn=1)
        data = json.loads(
            '{"type": "play_road_building_card", "payload": ['
            '[{"level": 0, "index": 0},{"level": 0, "index": 1} ],'
            '[{"level": 0, "index": 1},{"level": 0, "index": 2} ]'
            '] }')
        response = self.client.post('/games/' + str(self.game.id) + '/player/'
                                    + 'actions/', data, format='json')

        assert response.status_code == 400
        assert not Road.objects.filter(player=self.player1, vertex_index_A=0,
                                       vertex_level_A=0, vertex_index_B=1,
                                       vertex_level_B=0).exists()
        assert not Road.objects.filter(player=self.player1, vertex_index_A=1,
                                       vertex_level_A=0, vertex_index_B=2,
                                       vertex_level_B=0).exists()
        print('\033[32m',
              "Test actions (33/37) --> road_building_card_invalid_turn: OK",
              '\033[0m', sep='')


class TestVictoryPoint(TestCase):

    def setUp(self):
        self.board = Board.objects.create(name='board')
        self.game = Game.objects.create(name='colonos glaciales',
                                        board=self.board, currentTurn=4,
                                        dice1=2, dice2=3)
        self.user = User.objects.create_user(username='hercules',
                                             password='123qwe')
        self.user2 = User.objects.create_user(username='toshi',
                                              password='123qwe')
        self.user3 = User.objects.create_user(username='toshi1',
                                              password='123qwe')
        self.user4 = User.objects.create_user(username='toshi2',
                                              password='123qwe')

        self.session = Session.objects.create(user=self.user, tAccess='...',
                                              tRefresh='...')

        self.player3 = Player.objects.create(user=self.user, game=self.game,
                                             inTurn=False, colour='yellow')
        self.player4 = Player.objects.create(user=self.user2, game=self.game,
                                             inTurn=False, colour='white')

        self.user2 = authenticate(username=self.user3.username,
                                  password='123qwe')
        accessToken2 = (RefreshToken.for_user(self.user3)).access_token
        self.player2 = Player.objects.create(user=self.user3, game=self.game,
                                             inTurn=False, colour='blue')
        Resource.objects.create(player=self.player2, resource='lumber')
        Resource.objects.create(player=self.player2, resource='lumber')
        Settlement.objects.create(player=self.player2, level=2, index=6)

        self.client = APIClient()
        self.user = authenticate(username=self.user4.username,
                                 password='123qwe')
        accessToken = (RefreshToken.for_user(self.user4)).access_token
        self.session = Session.objects.create(
            tRefresh=(RefreshToken.for_user(self.user)), tAccess=accessToken,
            user=self.user)
        self.token = str(accessToken)
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        self.player1 = Player.objects.create(user=self.user4, game=self.game,
                                             inTurn=True, colour='red')
        Card.objects.create(player=self.player1, card_type='victory_point')

    def test_endpoint(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(self.token))

        data = json.loads('{"type": "play_victory_point_card", "payload": {}}')
        response = self.client.post('/games/' + str(self.game.id) + '/player/'
                                    + 'actions/', data, format='json')

        assert response.status_code == 202
        print('\033[32m',
              "Test actions (34/37) --> victory_point_card_endpoint: OK",
              '\033[0m', sep='')

    def test_get_victory_point(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(self.token))

        data = json.loads('{"type": "play_victory_point_card", "payload": {}}')
        response = self.client.post('/games/' + str(self.game.id) + '/player/'
                                    + 'actions/', data, format='json')

        assert Player.objects.get(inTurn=True).victoryPoint == 1

        print('\033[32m',
              "Test actions (35/37) --> victory_point_card_played: OK",
              '\033[0m', sep='')

    def test_win_with_card(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(self.token))
        self.player1.victoryPoint = 9
        self.player1.save()

        data = json.loads('{"type": "play_victory_point_card", "payload": {}}')
        response = self.client.post('/games/' + str(self.game.id) + '/player/'
                                    + 'actions/', data, format='json')

        assert Player.objects.get(inTurn=True).victoryPoint == 10
        assert Player.objects.filter(game=self.game,
                                     victoryPoint=10).exists() == True
        print('\033[32m',
              "Test actions (36/37) --> victory_point_card_win: OK",
              '\033[0m', sep='')

    def test_victory_point_card_delete_card(self):
        Card.objects.create(player=self.player1, card_type='victory_point')
        countCards = Card.objects.filter(player=self.player1,
                                         card_type='victory_point').count()

        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(self.token))

        data = json.loads('{"type": "play_victory_point_card", "payload": {}}')
        response = self.client.post('/games/' + str(self.game.id) + '/player/'
                                    + 'actions/', data, format='json')

        assert Card.objects.filter(player=self.player1,
                                   card_type='victory_point').count() < countCards
        print('\033[32m',
              "Test actions (37/37) --> victory_point_card_delete: OK",
              '\033[0m', sep='')
