from cards.models import Resource
from cards.models import validateResourcesCardsBuyCard, ValidateKnightCard, \
    validateRoadBuildingCard, \
    validateVictoryPointCard
from construction.models import validateResourcesRoad, \
    validateResourcesSettlement, availablePayloadSettlement
from game.models import validateDice, validateTurn


def validateTradeWithBank(player):
    valid = (Resource.objects.filter(player=player,
                                     resource='brick').count() >= 4 or
             Resource.objects.filter(player=player,
                                     resource='lumber').count() >= 4 or
             Resource.objects.filter(player=player,
                                     resource='wool').count() >= 4 or
             Resource.objects.filter(player=player,
                                     resource='grain').count() >= 4 or
             Resource.objects.filter(player=player,
                                     resource='ore').count() >= 4)
    return valid


def availableAction(action, player, game):
    if action == 'buy_card':
        return (validateResourcesCardsBuyCard(player) and
                validateDice(action, game) and validateTurn(action, game,
                                                            player) and player.inTurn)

    elif action == 'build_road':
        return (validateResourcesRoad(player) and validateDice(action,
                                                               game) and
                validateTurn(action, game, player) and player.inTurn)

    elif action == 'play_road_building_card':
        return (validateDice(action, game) and validateTurn(action, game,
                                                            player) and player.inTurn and
                validateRoadBuildingCard(player))

    elif action == 'build_settlement':
        return (validateResourcesSettlement(player) and
                validateDice(action, game) and validateTurn(action, game,
                                                            player)
                and player.inTurn and availablePayloadSettlement(player) != [])

    elif action == 'bank_trade':
        return (validateDice(action, game) and
                validateTurn(action, game, player) and validateTradeWithBank(
                    player) and player.inTurn)

    elif action == 'end_turn':
        return (validateDice(action, game) and validateTurn(action, game,
                                                            player) and player.inTurn)

    elif action == 'move_robber':
        return (validateDice(action, game) and validateTurn(action, game,
                                                            player) and not game.robber_moved and player.inTurn)

    elif action == 'play_knight_card':
        return (validateDice(action, game) and validateTurn(action, game,
                                                            player) and ValidateKnightCard(
            player)
                and player.inTurn)

    elif action == 'play_victory_point_card':
        return (validateDice(action, game) and validateTurn(action, game,
                                                            player) and validateVictoryPointCard(
            player)
                and player.inTurn)
