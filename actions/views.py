from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.utils import json
from rest_framework.views import APIView

from actions.models import availableAction
from cards.models import Resource, Card, buyCard, discardCards, stealResource
from catan_configuration.game_elements import GameElements
from construction.models import Settlement, Road
from construction.models import availablePayloadRoad, \
    availablePayloadSettlement
from game.models import Game, moveRobber, availablePayloadRobber
from player.models import Player
from player.service import endTurn


def tradeWithBank(player, offer, demand):
    if Resource.objects.filter(player=player, resource=offer).count() < 4:
        return False

    for i in range(4):
        Resource.objects.filter(resource=offer, player=player).first().delete()
    Resource.objects.create(player=player, resource=demand)
    return True


def callRobber(body, game, player):
    payload = body['payload']
    position = payload['position']
    level = position['level']
    index = position['index']
    steal_player = Player.objects.filter(user__username=payload['player'],
                                         game=game)
    steal_player = steal_player.first() if steal_player.count() == 1 else None
    moveRobber(game, level, index)
    stealResource(game, level, index, player, steal_player)


class ActionsView(APIView):
    """
    Aqui se llevan a cabo las distintas acciones, recibiendo el tipo de la
    accion y su payloads. Tambien se llevan a cabo comprobaciones, para
    determinar si una accion esta disponible para ejecutarse o no.
    """

    def get(self, request, pk, format=None):
        """
        Devuelve las acciones que se pueden jugar, tomando en cuenta el jugador,
        el turno y el estado del juego.
        """
        available_action = []

        game = Game.objects.get(id=pk)
        if game == None:
            raise ValidationError("El juego no existe")

        player = Player.objects.get(user=request.user, game=game)

        if player == None:
            raise ValidationError("El jugador no existe")

        if Player.objects.filter(game=game, victoryPoint=10).exists():
            return Response(available_action,
                            status=status.HTTP_200_OK)

        if availableAction('buy_card', player, game):
            action = {"type": 'buy_card', "payload": None}
            available_action.append(action)

        if availableAction('build_road', player, game):
            payload = availablePayloadRoad(player, game)
            action = {"type": 'build_road', "payload": payload}
            available_action.append(action)

        if availableAction('build_settlement', player, game):
            payload = availablePayloadSettlement(player)
            action = {"type": 'build_settlement', "payload": payload}
            available_action.append(action)

        if availableAction('bank_trade', player, game):
            action = {"type": 'bank_trade', "payload": None}
            available_action.append(action)

        if availableAction('end_turn', player, game):
            action = {"type": 'end_turn', "payload": None}
            available_action.append(action)

        if availableAction('move_robber', player, game):
            payload = availablePayloadRobber(game)
            action = {"type": 'move_robber', "payload": payload}
            available_action.append(action)

        if availableAction('play_knight_card', player, game):
            payload = availablePayloadRobber(game)
            action = {"type": 'play_knight_card', "payload": payload}
            available_action.append(action)

        if availableAction('play_road_building_card', player, game):
            payload = availablePayloadRoad(player, game)
            action = {"type": 'play_road_building_card', "payload": payload}
            available_action.append(action)

        if availableAction('play_victory_point_card', player, game):
            action = {"type": 'play_victory_point_card', "payload": None}
            available_action.append(action)

        return Response(available_action, status=status.HTTP_200_OK)

    def post(self, request, pk):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        type = body['type']
        if type not in GameElements.ACTIONS:
            return Response("La accion no existe",
                            status=status.HTTP_404_NOT_FOUND)

        game = Game.objects.get(id=pk)
        player = Player.objects.get(user=request.user, game=game)

        if player.inTurn == False:
            raise ValidationError("No es tu turno")

        if type == 'build_settlement':
            if not availableAction('build_settlement', player, game):
                return Response("La accion no esta disponible",
                                status=status.HTTP_400_BAD_REQUEST)
            else:
                payload = body['payload']
                level = payload['level']
                index = payload['index']
                settlement = Settlement(level=level,
                                        index=index,
                                        player=player)
                settlement.validateAndSave()
                return Response("Construccion realizada",
                                status=status.HTTP_202_ACCEPTED)

        elif type == 'build_road':
            if not availableAction('build_road', player, game):
                return Response("La accion no esta disponible",
                                status=status.HTTP_400_BAD_REQUEST)
            else:
                payload = body['payload']
                vertexA = payload[0]
                vertexB = payload[1]
                road = Road(player=player,
                            vertex_index_A=vertexA['index'],
                            vertex_index_B=vertexB['index'],
                            vertex_level_A=vertexA['level'],
                            vertex_level_B=vertexB['level'])
                road.validateAndSave()
                return Response("Camino construido",
                                status=status.HTTP_202_ACCEPTED)

        elif type == 'buy_card':
            if not availableAction('buy_card', player, game):
                return Response("La accion no esta disponible",
                                status=status.HTTP_400_BAD_REQUEST)
            else:
                card = Card(player=player)
                buyCard(card)
                return Response("Carta comprada",
                                status=status.HTTP_202_ACCEPTED)

        elif type == 'end_turn':
            if not availableAction('end_turn', player, game):
                return Response("La accion no esta disponible",
                                status=status.HTTP_400_BAD_REQUEST)
            else:
                endTurn(player, game)
                return Response("Turno terminado",
                                status=status.HTTP_202_ACCEPTED)

        elif type == 'bank_trade':
            if not availableAction('bank_trade', player, game):
                return Response("La accion no esta disponible",
                                status=status.HTTP_400_BAD_REQUEST)
            else:
                payload = body['payload']
                give = payload['give']
                receive = payload['receive']
                if tradeWithBank(player, give, receive):
                    return Response("Comercio completado",
                                    status=status.HTTP_202_ACCEPTED)
                else:
                    return Response("El comercio fallo",
                                    status=status.HTTP_400_BAD_REQUEST)

        elif type == 'move_robber':
            if not availableAction('move_robber', player, game):
                return Response("La accion no esta disponible",
                                status=status.HTTP_400_BAD_REQUEST)
            else:
                discardCards(game)
                callRobber(body, game, player)
                return Response("Ladron movido",
                                status=status.HTTP_202_ACCEPTED)

        elif type == 'play_knight_card':
            if not availableAction('play_knight_card', player, game):
                return Response("La accion no esta disponible",
                                status=status.HTTP_400_BAD_REQUEST)
            else:
                callRobber(body, game, player)
                Card.objects.filter(player=player,
                                    card_type='knight').first().delete()
                return Response("Carta Caballero jugada",
                                status=status.HTTP_202_ACCEPTED)

        elif type == 'play_road_building_card':
            if not availableAction('play_road_building_card', player, game):
                return Response("La accion no esta disponible",
                                status=status.HTTP_400_BAD_REQUEST)
            else:
                payload = body['payload']

                vertexLevelA_R1 = payload[0][0]['level']
                vertexIndexA_R1 = payload[0][0]['index']
                vertexLevelB_R1 = payload[0][1]['level']
                vertexIndexB_R1 = payload[0][1]['index']

                vertexLevelA_R2 = payload[1][0]['level']
                vertexIndexA_R2 = payload[1][0]['index']
                vertexLevelB_R2 = payload[1][1]['level']
                vertexIndexB_R2 = payload[1][1]['index']

                road1 = Road(player=player,
                             vertex_index_A=vertexIndexA_R1,
                             vertex_index_B=vertexIndexB_R1,
                             vertex_level_A=vertexLevelA_R1,
                             vertex_level_B=vertexLevelB_R1)
                road1.validateAndSave(card=True)

                road2 = Road(player=player,
                             vertex_index_A=vertexIndexA_R2,
                             vertex_index_B=vertexIndexB_R2,
                             vertex_level_A=vertexLevelA_R2,
                             vertex_level_B=vertexLevelB_R2)
                road2.validateAndSave(card=True)
                Card.objects.filter(player=player,
                                    card_type='road_building').first().delete()
                return Response("Carta Construir carretera jugada",
                                status=status.HTTP_202_ACCEPTED)

        elif type == 'play_victory_point_card':
            if not availableAction('play_victory_point_card', player, game):
                return Response("La accion no esta disponible",
                                status=status.HTTP_400_BAD_REQUEST)
            else:
                player.victoryPoint = player.victoryPoint + 1
                player.save()
                Card.objects.filter(player=player,
                                    card_type='victory_point').first().delete()
                return Response("Carta Puntos de victoria jugada",
                                status=status.HTTP_202_ACCEPTED)
