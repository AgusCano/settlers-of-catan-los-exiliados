from django.urls import path

from actions.views import ActionsView

urlpatterns = [
    path('games/<int:pk>/player/actions/', ActionsView.as_view()),
]
