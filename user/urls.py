from django.urls import path

from user.views import RegisterUserView

urlpatterns = [
    path('users/', RegisterUserView.as_view()),
]
