from django.test import TestCase
from django.test import tag
from rest_framework.test import APIClient


class TestRegister(TestCase):
    @tag('LIGHT')
    def test_register(self):
        client = APIClient()
        response = client.post('/users/', {'user': 'USERNAME',
                                           'pass': 'PASSWORD'})
        assert response.status_code == 201
        print('\033[32m', "Test registro (1/2) --> register: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_register_repeat(self):
        client = APIClient()
        response = client.post('/users/', {'user': 'USERNAME1',
                                           'pass': 'PASSWORD1'})
        assert response.status_code == 201

        client = APIClient()
        response = client.post('/users/', {'user': 'USERNAME1',
                                           'pass': 'PASSWORD2'})

        assert response.status_code == 400
        print('\033[32m', "Test registro (2/2) --> error_name_repeted: OK",
              '\033[0m', sep='')
