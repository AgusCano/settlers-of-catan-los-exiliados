from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response

from user.models import User


class RegisterUserView(generics.GenericAPIView):
    permission_classes = []

    def post(self, request, *args, **kwargs):
        repeatedUserName = User.objects.filter(
            username=request.data['user']).first()
        if repeatedUserName is not None:
            return Response("El usuario ya esta registrado",
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            User.objects.create_user(username=request.data['user'],
                                     password=request.data['pass'])
            return Response("Usuario registrado",
                            status=status.HTTP_201_CREATED)
