from django.contrib.auth import authenticate
from django.test import TestCase
from django.test import tag
from rest_framework.test import APIClient
from rest_framework.utils import json
from rest_framework_simplejwt.tokens import RefreshToken

from board.models import Board, Hex
from cards.models import Card, Resource, stealResource
from catan_configuration.game_elements import GameElements
from construction.models import Settlement
from game.models import Game
from player.models import Player
from room.models import Room
from session.models import Session
from user.models import User


class getCardsTest(TestCase):
    """
    Para agilizar testeo hay dos clases de test: 'DEBUG' y 'LIGHT'
    - 'LIGHT': Solo hace asserts de los test e imprime si los tests pasan.
    - 'DEBUG': Debug prueba todos los test e imprime en consola lo que los
       test hicieron para mas detalle a la hora de debuguear.
    """

    @tag('LIGHT')
    def setUp(self):
        self.user = User.objects.create_user(username='pepe',
                                             password='123qwe')

        self.client = APIClient()
        self.user = authenticate(username=self.user.username,
                                 password='123qwe')
        self.accessToken = (RefreshToken.for_user(self.user)).access_token
        self.session = Session.objects.create(
            tRefresh=RefreshToken.for_user(self.user),
            tAccess=str(self.accessToken),
            user=self.user)
        self.client.credentials(
            HTTP_AUTHORIZATION='Bearer ' + str(self.accessToken))

        self.create_cards = self.client.get('')
        self.save_cards = self.client.get('')

    @tag('LIGHT')
    def test_get_cards(self):
        """
        Crea un user, un game, un player, una cards y un recurso
        para hacer request y testear sobre la respuesta.
        """
        self.board = Board.objects.create(name='BOARDNAME')
        self.game = Game.objects.create(name='GAMENAME',
                                        board=self.board)
        self.player = Player.objects.create(game=self.game,
                                            user=self.user)
        self.card = Card.objects.create(card_type='CARDTYPE',
                                        player=self.player)
        self.resource = Resource.objects.create(resource='RESOURCE',
                                                player=self.player)
        response = self.client.get('/games/' + str(self.game.id) + '/player/')
        assert response.status_code == 200
        self.create_cards = response
        print('\033[32m', "Test cards (1/2) --> get_card: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_save_cards(self):
        self.board = Board.objects.create(name='BOARDNAME')
        self.game = Game.objects.create(name='GAMENAME',
                                        board=self.board)
        self.player = Player.objects.create(game=self.game,
                                            user=self.user)
        self.card = Card.objects.create(card_type='CARDTYPE',
                                        player=self.player)
        self.resource = Resource.objects.create(resource='RESOURCE',
                                                player=self.player)

        response = self.client.get('/games/' + str(self.game.id) + '/player/')
        assert response.status_code == 200
        self.save_cards = response
        responseJSON = response.json()
        assert (responseJSON['resources'][0] == 'RESOURCE'
                and responseJSON['cards'][0] == 'CARDTYPE')
        print('\033[32m', "Test cards (2/2) --> saved_card: OK",
              '\033[0m', sep='')

    @tag('DEBUG')
    def test_debug_mode(self):
        self.test_get_cards()
        self.test_save_cards()
        print(json.dumps(self.create_cards.json(), indent=4))
        print(json.dumps(self.save_cards.json(), indent=4))


class TestStealResource(TestCase):
    """
    Para agilizar testeo hay dos clases de test: 'DEBUG' y 'LIGHT'
    - 'LIGHT': Solo hace asserts de los test e imprime si los tests pasan.
    - 'DEBUG': Debug prueba todos los test e imprime en consola lo que los
       test hicieron para mas detalle a la hora de debuguear.
    """

    @tag('LIGHT')
    def setUp(self):
        self.board = Board.objects.create(name='Shinsengumi')
        for elem in GameElements.BOARD_MAP:
            Hex.objects.create(board=self.board, level=elem[0], index=elem[1],
                               terrain='brick', token=69)
        self.user1 = User.objects.create_user(username='Toshi',
                                              password='mayo1234')
        self.user2 = User.objects.create_user(username='Pote',
                                              password='mayo1234')
        self.user3 = User.objects.create_user(username='Hercules',
                                              password='mayo1234')
        self.user4 = User.objects.create_user(username='Mechi',
                                              password='mayo1234')
        self.session1 = Session.objects.create(tRefresh='rt1', tAccess='at1',
                                               user=self.user1)
        self.session2 = Session.objects.create(tRefresh='rt2', tAccess='at2',
                                               user=self.user2)
        self.session3 = Session.objects.create(tRefresh='rt3', tAccess='at3',
                                               user=self.user3)
        self.session4 = Session.objects.create(tRefresh='rt4', tAccess='at4',
                                               user=self.user4)
        self.room = Room.objects.create(name='Shiro', max_players=3,
                                        state='Playing', game_has_started=True)
        self.game = Game.objects.create(name='Another', currentTurn=5,
                                        board=self.board)
        self.player1 = Player.objects.create(user=self.user1, room=self.room,
                                             colour='blue', inTurn=True,
                                             game=self.game)
        self.player2 = Player.objects.create(user=self.user2, room=self.room,
                                             colour='red', inTurn=False,
                                             game=self.game)
        self.player3 = Player.objects.create(user=self.user3, room=self.room,
                                             colour='yellow', inTurn=False,
                                             game=self.game)
        self.player4 = Player.objects.create(user=self.user4, room=self.room,
                                             colour='white', inTurn=False,
                                             game=self.game)

        for i in range(2):
            Resource.objects.create(resource='lumber', player=self.player1)

        for i in range(3):
            Resource.objects.create(resource='lumber', player=self.player2)

        Settlement.objects.create(level=1, index=3, player=self.player1)
        Settlement.objects.create(level=1, index=5, player=self.player2)
        Settlement.objects.create(level=0, index=1, player=self.player3)
        Settlement.objects.create(level=2, index=1, player=self.player4)
        self.level = 1
        self.index = 1

    @tag('LIGHT')
    def test_myself(self):
        stealResource(self.game, self.level, self.index, self.player1,
                      self.player1)
        assert Resource.objects.filter(player=self.player1).count() == 2
        print('\033[32m',
              "Test steal_resource (1/4) --> cant_steal_resource_to_me: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_from_player_without_cards(self):
        stealResource(self.game, self.level, self.index, self.player1,
                      self.player3)
        assert Resource.objects.filter(player=self.player1).count() == 2
        print('\033[32m',
              "Test steal_resource (2/4) --> steal_resource_from_player_without_cards: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_random_player(self):
        stealResource(self.game, self.level, self.index, self.player1)
        assert Resource.objects.filter(player=self.player1).count() == 3
        assert Resource.objects.filter(player=self.player2).count() == 2
        print('\033[32m',
              "Test steal_resource (3/4) --> steal_resource_random_player_in_hex: OK",
              '\033[0m', sep='')

    @tag('LIGHT')
    def test_from_player_not_in_hex(self):
        stealResource(self.game, self.level, self.index, self.player1,
                      self.player4)
        assert Resource.objects.filter(player=self.player2).count() == 3
        print('\033[32m',
              "Test steal_resource (3/4) --> steal_resource_player_not_in_hex: OK",
              '\033[0m', sep='')
