import random
from math import floor

from django.db import models
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from catan_configuration.game_elements import GameElements
from game.models import Game
from player.models import Player


def vertexsOfHex(level, index):
    if level == 0:
        return [(0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5)]
    if level == 1:
        return [(0, index), (0, 0 if index + 1 == 6 else index + 1),
                (1, 3 * index), (1, 3 * index + 1),
                (1, 3 * index + 2),
                (1, 0 if 3 * index + 3 == 18 else 3 * index + 3)]
    if level == 2:
        if index % 2 == 0:
            alfa = index + index / 2
            beta = index + (3 * index) / 2
            return [(1, alfa), (1, alfa + 1), (1, alfa - 1), (2, beta),
                    (2, beta + 1), (2, beta - 1)]
        else:
            alfa = index + (index - 1) / 2
            beta = alfa + index - 1
            return [(1, alfa), (1, alfa + 1), (2, beta), (2, beta + 1),
                    (2, beta + 2), (2, beta + 3)]


def ValidateKnightCard(player):
    return Card.objects.filter(player=player, card_type='knight').exists()


def validateRoadBuildingCard(player):
    return Card.objects.filter(player=player,
                               card_type='road_building').exists()


def validateVictoryPointCard(player):
    return Card.objects.filter(player=player,
                               card_type='victory_point').exists()


def stealResource(game, level, index, robber_player, steal_player=None):
    from construction.models import Settlement
    vertexList = vertexsOfHex(level, index)

    player = None
    if steal_player != None:
        countSet = 0
        for vertex in vertexList:
            countSet = countSet + Settlement.objects.filter(level=vertex[0],
                                                            index=vertex[1],
                                                            player=steal_player).count()

        if countSet == 0:
            return Response(
                "El jugador elegido no se encuentra cerca del ladron",
                status=status.HTTP_406_NOT_ACCEPTABLE)

        if Resource.objects.filter(player=steal_player).count() == 0:
            return Response("El jugador elegido no tiene recursos",
                            status=status.HTTP_406_NOT_ACCEPTABLE)

        player = steal_player

    else:
        players = Player.objects.filter(game=game).exclude(
            user=robber_player.user)
        for elem in players:
            for vertex in vertexList:
                if Settlement.objects.filter(level=vertex[0], index=vertex[1],
                                             player=elem).count() > 0 and Resource.objects.filter(
                    player=elem).count() > 0:
                    player = elem

    if player == None or player == robber_player:
        return Response("No hay jugadores para robar al alcance del ladron",
                        status=status.HTTP_406_NOT_ACCEPTABLE)

    # Guardo el tipo del primer recurso, borro la carta,
    # creo una nueva con ese tipo y se la asigno al jugador del turno
    typeResource = Resource.objects.filter(player=player).first().resource
    Resource.objects.filter(player=player).first().delete()
    Resource.objects.create(resource=typeResource, player=robber_player)
    return Response("Robo completado", status=status.HTTP_202_ACCEPTED)


def validateResourcesCardsBuyCard(player):
    return (Resource.objects.filter(player=player,
                                    resource='grain').count() > 0 and (
                    Resource.objects.filter(player=player,
                                            resource='wool').count() > 0) and (
                    Resource.objects.filter(player=player,
                                            resource='ore').count() > 0))


def selectCard(player):
    card_types = ['knight', 'victory_point', 'monopoly', 'year_of_plenty',
                  'road_building']
    usedDeck = Card.objects.filter(card_type__in=card_types,
                                   player__in=Player.objects.filter(
                                       game=player.game))

    selectableDeck = [item for item in GameElements.CARD_DECK if
                      item not in usedDeck]

    if len(selectableDeck) > 0:
        randCard = random.randrange(0, len(selectableDeck))
        return selectableDeck[randCard]
    else:
        return ''


def discardCards(game):
    if game.dice1 + game.dice2 == 7:
        players_in_game = Player.objects.filter(game=game)
        if players_in_game is not None:
            for player in players_in_game:
                if player.resources.count() > 7:
                    resources = Resource.objects.filter(player=player.id)
                    discardNum = int(floor(player.resources.count() / 2))
                    for i in range(discardNum):
                        resources[i].delete()


def buyCard(self):
    card_type = selectCard(self.player)
    if self.player.inTurn:
        self.card_type = card_type
        self.save()
        Resource.objects.filter(resource='grain',
                                player=self.player).first().delete()
        Resource.objects.filter(resource='wool',
                                player=self.player).first().delete()
        Resource.objects.filter(resource='ore',
                                player=self.player).first().delete()
    else:
        raise ValidationError("No se pudo realizar la compra")


class Resource(models.Model):
    resource = models.CharField(max_length=1, choices=GameElements.RESOURCE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE,
                               related_name='resources')


class Card(models.Model):
    card_type = models.CharField(max_length=1,
                                 choices=GameElements.CARD_TYPE)
    player = models.ForeignKey(Player,
                               on_delete=models.CASCADE,
                               related_name='cards')
    played = models.BooleanField(default=False)
