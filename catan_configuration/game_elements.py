class GameElements():
    RESOURCE = (
        ('B', 'brick'),
        ('L', 'lumber'),
        ('W', 'wool'),
        ('G', 'grain'),
        ('O', 'ore')
    )

    TERRAIN = (
        ('B', 'brick'),
        ('L', 'lumber'),
        ('W', 'wool'),
        ('G', 'grain'),
        ('O', 'ore'),
        ('D', 'desert')
    )

    CARD_TYPE = (
        ('R', 'road_building'),  # P
        ('Y', 'year_of_plenty'),  # P
        ('M', 'monopoly'),  # P
        ('V', 'victory_point'),
        ('K', 'knight'),
        ('A', 'great_army'),
        ('C', 'commercial_route')
    )

    CARD_DECK = ['knight', 'knight', 'knight', 'knight', 'knight', 'knight',
                 'knight', 'knight', 'knight', 'knight', 'knight', 'knight',
                 'knight', 'knight', 'victory_point', 'victory_point',
                 'victory_point',
                 'victory_point', 'victory_point', 'year_of_plenty',
                 'year_of_plenty',
                 'monopoly', 'monopoly', 'road_building', 'road_building']

    BOARD_MAP = [
        (0, 0),
        (1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (1, 5),
        (2, 0), (2, 1), (2, 2), (2, 3), (2, 4), (2, 5), (2, 6), (2, 7),
        (2, 8), (2, 9), (2, 10), (2, 11)
    ]

    VERTEX_MAP = [
        (0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5),

        (1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (1, 7),
        (1, 8), (1, 9), (1, 10), (1, 11), (1, 12), (1, 13), (1, 14), (1, 15),
        (1, 16), (1, 17),

        (2, 0), (2, 1), (2, 2), (2, 3), (2, 4), (2, 5), (2, 6), (2, 7),
        (2, 8), (2, 9), (2, 10), (2, 11), (2, 12), (2, 13), (2, 14), (2, 15),
        (2, 16),
        (2, 17), (2, 18), (2, 19), (2, 20), (2, 21), (2, 22), (2, 23), (2, 24),
        (2, 25), (2, 26), (2, 27), (2, 28), (2, 29)
    ]

    ROOM_STATUS = (
        ('A', 'Available'),
        ('P', 'Playing')
    )

    COLOUR = (
        ('red', 'red'),
        ('blue', 'blue'),
        ('yellow', 'yellow'),
        ('white', 'white')
    )

    ACTIONS = ['build_settlement', 'upgrade_city', 'build_road', 'move_robber',
               'buy_card', 'play_knight_card', 'play_road_building_card',
               'play_monopoly_card', 'play_year_of_plenty_card', 'end_turn',
               'bank_trade', 'play_victory_point_card']
